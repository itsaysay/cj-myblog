<#include "base.ftl" />
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="${view.seoKeywords!}">
    <meta name="description" content="${view.seoDescriptions!}">
    <title>${web.title!}-${view.seoTitle!}</title>

<@style/>

</head>

<body>

<div id="wrapper">

    <div class="gray-bg">
            <@header/>

            <div class="wrapper wrapper-content  article">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="pull-right">
                                    <#list view.tagDoList as item>
                                    <button class="btn btn-white btn-xs" type="button">${item.tag!}</button>
                                    </#list>
                                </div>
                                <div class="text-center article-title">
                                    <span class="text-muted"><i class="fa fa-clock-o"></i> ${view.createTime!}</span>
                                    <h2>
                                        ${view.title!}
                                    </h2>
                                </div>
                                <p>
                                    ${view.content!}

                                </p>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>Tags:</h5>
                                        <#list view.tagDoList as item>
                                        <button class="btn btn-primary btn-xs" type="button">${item.tag!}</button>
                                        </#list>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="small text-right">
                                            <h5>Stats:</h5>
                                            <div> <i class="fa fa-comments-o"> </i> ${view.commentCount!} 评论 </div>
                                            <i class="fa fa-eye"> </i> ${view.hits!} 阅读
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h3>发表评论</h3>

                                        <form  class="form-horizontal" id="commentForm">
                                            <input type="hidden" name="artId" value="${view.id!}"/>
                                            <div class="form-group"><label class="col-sm-2">名称*</label>
                                                <div class="col-sm-5"><input type="text" name="name" id="name" class="form-control" required></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2">邮箱*</label>
                                                <div class="col-sm-5"><input type="text" name="email"  id="email" class="form-control" required></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2" id="codeLabel">验证码*</label>
                                                <div class="col-sm-5"><input type="text" name="verifyCode" class="form-control" required></div>
                                                <div class="col-sm-2 pull-left"><img src="/verifyCode" onclick="changeImg()" id="codeImg" style="width: 100px;height: 30px"/></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-12" id="commentLabel">正文*(最长字数100字)</label>
                                                <div class="col-sm-12">
                                                    <textarea class="form-control  m-t Input_text" rows="7" required name="comment" id="comment"></textarea>
                                                </div>
                                                <br/>
                                                <div class="col-sm-12" style="margin-top:5px">
                                                    <div class="faceDiv"> </div>
                                                    <div class="Input_Foot">
                                                        <a class="imgBtn fa fa-smile-o" href="javascript:void(0);"></a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" style="margin-top:30px">
                                                    <button type="submit" class="btn btn-primary">提交</button>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" name="remember" value="1">记住我
                                                    </label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-lg-12">

                                        <h3>评论:</h3>
                                        <#list commentList as list>
                                        <div class="social-feed-box">
                                            <div class="social-avatar">
                                                <a href="" class="pull-left">
                                                    <img alt="image" src="${list.avatar!}">
                                                </a>
                                                <div class="media-body">
                                                    <a href="#" id="${list.name}${list.id}">
                                                        ${list.name!}
                                                    </a>
                                                    <small class="text-muted">${list.createTime!}</small>
                                                </div>
                                            </div>
                                            <div class="social-body">
                                                <p>
                                                    ${list.comment!}
                                                </p>
                                            </div>
                                            <#list list.commentDos as comments>
                                                <div class="social-footer ">
                                                    <div class="social-avatar">
                                                        <a href="" class="pull-left">
                                                            <img alt="image" src="${comments.avatar!}">
                                                        </a>
                                                        <div class="media-body">
                                                            <a href="#">
                                                            ${comments.name!}
                                                            </a>
                                                            <small class="text-muted">${comments.createTime!}</small>
                                                        </div>
                                                    </div>
                                                    <div class="social-body">
                                                        <p>
                                                        ${comments.comment!}
                                                        </p>
                                                    </div>
                                                </div>
                                            </#list>
                                        </div>
                                        </#list>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div id="small-directory">
                    <ul id="zTree" class="ztree">
                    </ul>
                </div>
            </div>
        <div class="footer">
            <div style="text-align: center">
                ${web.footer!}
            </div>
        </div>
    </div>
</div>

<@js/>


<script>
    //表情
    ImgIputHandler.Init();
    ImgIputHandler.themePath = "${themePath}";

    //树形目录
    var setting = {
        view: {
            showIcon: false

        },
        data: {
            simpleData: {
                enable: true
            }
        }
    };

    var zNodes =eval(${view.directory!});

    $.fn.zTree.init($("#zTree"), setting, zNodes);


    var name = localStorage.getItem("name");
    var email = localStorage.getItem("email");
    if (name != "null") $("#name").val(name);
    if (email != null)  $("#email").val(email);


    $("#commentForm").on("submit",function () {

        if ($("input[name='remember']:checked").length>0){
            localStorage.setItem("name",$("#name").val());
            localStorage.setItem("email",$("#email").val());
        }

        $("span").remove(".errorClass");
        $("br").remove(".errorClass");

        var comment = $("#comment").val();
        if (comment.length >100){
           $("#commentLabel").prepend('<span class="errorClass" style="color:red">*评论不能超过100字</span><br class="errorClass"/>');
            return false;
        }

        $.post("/commentSave",$("#commentForm").serialize(),function (result) {
            if (result.code != 0){
                $("#codeLabel").prepend('<span class="errorClass" style="color:red">*'+result.msg+'</span><br class="errorClass"/>');
                return false;
            }else {
                location.reload();
            }
        });

        return false;
    })

    //更换验证码
    function changeImg() {
        var imgSrc = $("#codeImg");
        var src = imgSrc.attr("src");
        imgSrc.attr("src", changeUrl(src));
    }
    //为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
    function changeUrl(url) {
        var timestamp = (new Date()).valueOf();
        var index = url.indexOf("?",url);
        if (index > 0) {
            url = url.substring(0, url.indexOf("?",url));
        }
        if ((url.indexOf("&") >= 0)) {
            url = url + "×tamp=" + timestamp;
        } else {
            url = url + "?timestamp=" + timestamp;
        }
        return url;
    }

    var contextPath = "${themePath}";

    $(document).on("click",".face",function(event){ //添加表情那个链接的class名用来当做点击的触发事件
        if(! $('#sinaEmotion').is(':visible')){  //这句不用管，有兴趣可以看js源代码里
            var facebox = $(this).parents(".face_box").find(".face_content");  //定义facebox是用来装表情的div框框
            $(this).sinaEmotion(facebox);   //把表情放到上面定义的容器框框里
        }
    });

</script>

</body>
</html>
