<#include "base.ftl" />
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="${web.keyWords!}">
    <meta name="description" content="${web.description!}">
    <title>${web.title!}</title>

<@style/>

</head>

<body>

<div id="wrapper">

    <div class="gray-bg dashbard-1">
        <@header/>

            <div class="wrapper wrapper-content">
                <div class="row">
                    <#list list as item>
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="ibox">
                            <div class="ibox-content">
                                <a href="/view/${item.id}" class="btn-link">
                                    <h2>
                                        ${item.title!}
                                    </h2>
                                    <#if item.status == 2><span class="btn-warning">置顶</span></#if>
                                </a>
                                <div class="small m-b-xs">
                                    <strong>${item.anchor!}</strong> <span class="text-muted"><i class="fa fa-clock-o"></i>${item.createTime!}</span>
                                </div>
                                <p>
                                    ${item.subject!}
                                </p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>标签:</h5>
                                        <#list item.tagDoList as tag>
                                        <a class="btn btn-primary btn-xs" href="">${tag.tag!}</a>
                                        </#list>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="small text-right">
                                            <h5>状态:</h5>
                                            <div> <i class="fa fa-comments-o"> </i> ${item.commentCount!} 评论 </div>
                                            <i class="fa fa-eye"> </i> ${item.hits!} 阅读
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </#list>
                </div>
            </div>
        <div class="footer">
            <div style="text-align: center">
                ${web.footer!} ${web.footerOther!}
            </div>
        </div>
    </div>
</div>

<@js/>

<script>

</script>

</body>
</html>
