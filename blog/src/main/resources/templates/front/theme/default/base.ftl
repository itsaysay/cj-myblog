<#macro style>

    <link href="${themePath}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${themePath}/static/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${themePath}/static/css/animate.css" rel="stylesheet">
    <link href="${themePath}/static/css/style.css" rel="stylesheet">
    <link href="${themePath}/static/css/ztree/metroStyle/metroStyle.css" rel="stylesheet">
    <link href="${themePath}/static/css/face.css" rel="stylesheet">

</#macro>


<#macro js>
    <script src="${themePath}/static/js/jquery-2.1.1.js"></script>
    <script src="${themePath}/static/js/bootstrap.min.js"></script>
    <script src="${themePath}/static/js/ztree/jquery.ztree.core.min.js"></script>
    <script src="${themePath}/static/js/face.js"></script>
    <script>
        (function(){
            var bp = document.createElement('script');
            var curProtocol = window.location.protocol.split(':')[0];
            if (curProtocol === 'https') {
                bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
            }
            else {
                bp.src = 'http://push.zhanzhang.baidu.com/push.js';
            }
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(bp, s);
        })();
    </script>

</#macro>

<#macro header>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12" style="padding-left: 50px">
            <h1><a href="${web.url!}" target="_self">${web.blogName!}</a> <h6>${web.blogSubname!}</h6></h1>
        </div>
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li>
                    <a href="/">首页</a>
                </li>
                <#list partList as part>
                    <li>
                        <#if (part.outUrl??) && (part.outUrl != "")>
                            <a href="${part.outUrl}" target="_blank">${part.name!}</a>
                        <#else>
                            <a href="${part.url}">${part.name!}</a>
                        </#if>
                    </li>
                </#list>
            </ol>

        </div>
    </div>
</#macro>