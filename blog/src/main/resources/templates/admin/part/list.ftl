<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>栏目管理</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/index">首页</a>
            </li>
            <li class="active">
                <strong>栏目管理</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>所有栏目</h5>
                    <div class="ibox-tools">
                        <a target="modal" modal="lg" href="/admin/part/add" class="btn btn-primary btn-xs">新建栏目</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <!--搜索-->
                    <!--<div class="row">
                        <div class="col-sm-3 m-b-xs">
                            <select class="input-sm form-control input-s-sm inline">
                            <option value="0">Option 1</option>
                            <option value="1">Option 2</option>
                            <option value="2">Option 3</option>
                            <option value="3">Option 4</option>
                        </select>
                        </div>

                        <div class="col-sm-3">
                                <input type="text" placeholder="Search" class="input-sm form-control">
                        </div>

                        <div class="col-sm-6">
                            <button type="button" class="btn btn-sm btn-primary"> Go!</button>
                        </div>

                    </div>-->

                    <!--表格列表-->
                    <div class="table-responsive">
                        <table class="table table-striped" id="tb_list">
                            <thead>
                            <tr>
                                <th>序号</th>
                                <th>名称</th>
                                <th>文章</th>
                                <th>状态</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var tb_list;
    $(function () {


        tb_list = $('#tb_list').DataTable({
            "dom":'itflp',
            "processing":true,
            "searching":false,
            "paging":false,
            "bInfo":false,
            "language":{"url":"/admin/static/js/plugins/jQdatatables/language.json"},
            "ajax":{"url":"/admin/part/page","type":"post"},
            "columns":[
                {"data":"id"},
                {"data":"name"},
                {"data":"count"},
                {"data":"status"},
                {"data":null}
            ],
            "columnDefs" : [
                {
                    "targets" : -2,
                    "data" : null,
                    "render" : function(data) {
                        if (data == 1){
                            return "启用";
                        }else {
                            return "停用";
                        }
                    }
                },
                {
                    "targets" : -1,
                    "data" : null,
                    "render" : function(data) {
                        var btn = ' <a  class="btn btn-xs btn-success" target="modal" modal="lg" href="/admin/part/edit?id='+data.id+'">修改</a>&nbsp;'
                                +'<a class="btn btn-xs btn-danger" callback="listReload();" data-body="确认要删除吗？该操作会删除栏目下所有文章" target="ajaxTodo" href="/admin/part/delete?id='+data.id+'">删除</a>';
                        return btn;
                    }
                }
            ]
        }).on('xhr.dt', function ( e, settings, data ,xhr) {
            dataTableMsg(data,xhr);
        } );

    });

    function listReload() {
        var param = {
        };
        tb_list.settings()[0].ajax.data = param;
        tb_list.ajax.reload();
    }

</script>