<div class="row">
    <div class="col-md-12">
        <form id="addForm">
            <div class="box-body">
                <div class="form-group">
                    <label>名称</label>
                    <input type="text" class="form-control" name="name" id="name" value="">
                </div>
                <div class="form-group">
                    <label>外链地址</label>
                    <input type="text" class="form-control" name="outUrl" id="outUrl" value="" >
                </div>
                <div class="form-group">
                    <label>状态</label>
                    <select name="status" class="form-control select2" style="width: 100%;">
                        <option  value="1">启用</option>
                        <option  value="0">停用</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                <button type="button" onclick="partSave()" class="btn btn-primary">保存</button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    function partSave(){
        $.ajax({
            url: '/admin/part/save',
            type: 'post',
            dataType: 'text',
            data: $("#addForm").serialize(),
            success: function (data) {
                listReload();
                $("#lgModal").modal('hide');
                alertMsg("更新成功","success");
            }
        });
    }

</script>