<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>评论管理</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/index">首页</a>
            </li>
            <li class="active">
                <strong>评论管理</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>所有评论</h5>
                </div>
                <div class="ibox-content">
                    <!--搜索-->
                    <#--<div class="row">
                        <div class="col-sm-3 m-b-xs">
                            <select class="input-sm form-control input-s-sm inline">
                            <option value="0">Option 1</option>
                            <option value="1">Option 2</option>
                            <option value="2">Option 3</option>
                            <option value="3">Option 4</option>
                        </select>
                        </div>

                        <div class="col-sm-3">
                                <input type="text" placeholder="Search" class="input-sm form-control">
                        </div>

                        <div class="col-sm-6">
                            <button type="button" class="btn btn-sm btn-primary"> Go!</button>
                        </div>

                    </div>-->

                    <!--表格列表-->
                    <div class="table-responsive">
                        <table class="table table-striped" id="tb_list">
                            <thead>
                            <tr>
                                <th>评论人</th>
                                <th>评论时间</th>
                                <th width="40%">评论内容</th>
                                <th width="20%">所属文章</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var tb_list;
    $(function () {


        tb_list = $('#tb_list').DataTable({
            "processing":true,
            "searching":false,
            "paging":true,
            "bServerSide":true,
            "bInfo":false,
            "bLengthChange":false,
            "bSort":false,
            "language":{"url":"/admin/static/js/plugins/jQdatatables/language.json"},
            "ajax":{"url":"/admin/comment/page","type":"post"},
            "columns":[
                {"data":"name"},
                {"data":"createTime"},
                {"data":"comment"},
                {"data":"artName"},
                {"data":null}
            ],
            "columnDefs" : [
                {
                    "targets" : -1,
                    "data" : null,
                    "render" : function(data) {
                        var btn = ' <a  class="btn btn-xs btn-success" target="modal" modal="lg" href="/admin/comment/edit?id='+data.id+'">回复</a>&nbsp;'
                                +'<a class="btn btn-xs btn-danger" callback="listReload();" data-body="确认要删除吗？" target="ajaxTodo" href="/admin/comment/delete?id='+data.id+'">删除</a>';
                        return btn;
                    }
                }
            ]
        }).on('xhr.dt', function ( e, settings, data ,xhr) {
            dataTableMsg(data,xhr);
        } );

    });

    function listReload() {
        var param = {
        };
        tb_list.settings()[0].ajax.data = param;
        tb_list.ajax.reload();
    }

</script>