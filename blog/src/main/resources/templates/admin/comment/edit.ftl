<div class="row">
    <div class="col-md-12">
        <form id="addForm" class="form-horizontal">
            <div class="box-body">
                <input type="hidden" name="artId" value="${bean.artId}">
                <input type="hidden" name="commentId" value="${bean.id}">
                <input type="hidden" name="toUser" value="${bean.email!}">
                <input type="hidden" name="toName" value="${bean.name!}">
                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control  m-t Input_text" rows="7" required name="comment" id="comment"></textarea>
                    </div>
                    <div class="col-sm-12" style="margin-top:5px">
                        <div class="faceDiv"> </div>
                        <div class="Input_Foot">
                            <a class="imgBtn fa fa-smile-o" href="javascript:void(0);"></a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox checkbox-primary">
                            <input name="sendMail" type="checkbox" value="1" checked><label>是否发送提醒邮件</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="margin-top: 30px;">
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                <button type="button" onclick="commentSave()" class="btn btn-primary">保存</button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    //表情
    ImgIputHandler.Init();
    ImgIputHandler.themePath = "/admin";


    function commentSave(){
        $.ajax({
            url: '/admin/comment/save',
            type: 'post',
            dataType: 'text',
            data: $("#addForm").serialize(),
            success: function (data) {
                listReload();
                $("#lgModal").modal('hide');
                alertMsg("更新成功","success");
            }
        });
    }

</script>