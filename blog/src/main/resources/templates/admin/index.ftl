<#include "base.ftl" />
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>管理后台</title>

  <@style/>

</head>

<body>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <@menu/>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <@head/>
        </div>
        <div id="main-content">
            <div class="wrapper wrapper-content">
                <div class="col-lg-12">欢迎登录管理后台</div>
            </div>
        </div>
        <div class="footer">
            <@footer/>
        </div>
    </div>
    <@chat/>
</div>

<@js/>

<script>

</script>

</body>
</html>
