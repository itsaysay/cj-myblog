<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>网站设置</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/index">首页</a>
            </li>
            <li>
                <a>系统设置</a>
            </li>
            <li class="active">
                <strong>网站设置</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-md-12">
        <div class="ibox">
            <div class="ibox-content">
                <form id="editForm">
                    <div class="box-body">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1">网站设置</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-2">邮件设置</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label>博客名称</label>
                                            <input type="text" class="form-control" name="blogName" id="blogName" value="${bean.blogName!}">
                                        </div>
                                        <div class="form-group">
                                            <label>博客签名</label>
                                            <input type="text" class="form-control" name="blogSubname" id="blogSubname" value="${bean.blogSubname!}" >
                                        </div>
                                        <div class="form-group">
                                            <label>博客网址</label>
                                            <input type="text" class="form-control" name="url" id="url" value="${bean.url!}" >
                                        </div>
                                        <div class="form-group">
                                            <label>首页标题</label>
                                            <input type="text" class="form-control" name="title" id="title" value="${bean.title!}" >
                                        </div>
                                        <div class="form-group">
                                            <label>Meta关键词</label>
                                            <input type="text" class="form-control" name="keyWords" id="keyWords" value="${bean.keyWords!}" >
                                        </div>
                                        <div class="form-group">
                                            <label>Meta描述</label>
                                            <input type="text" class="form-control" name="description" id="description" value="${bean.description!}" >
                                        </div>
                                        <div class="form-group">
                                            <label>版权信息</label>
                                            <input type="text" class="form-control" name="footer" id="footer" value='${bean.footer!}' >
                                        </div>
                                        <div class="form-group">
                                            <label>底部其他信息</label>
                                            <textarea class="form-control" name="footerOther" rows="10">${bean.footerOther!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-2" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label>邮件服务器</label>
                                            <input type="text" class="form-control" name="mailServer" value='${bean.mailServer!}' >
                                        </div>
                                        <div class="form-group">
                                            <label>邮箱用户名</label>
                                            <input type="text" class="form-control" name="mailUser" value='${bean.mailUser!}' >
                                        </div>
                                        <div class="form-group">
                                            <label>邮箱密码</label>
                                            <input type="password" class="form-control" name="mailPassword" value='${bean.mailPassword!}' >
                                        </div>
                                        <div class="form-group">
                                            <label>评论回复邮件模板</label>
                                            <div id="editor" style="height: 300px"></div>
                                            <textarea style="display: none;" name="mailTpl" id="mailTpl"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="editSave()" class="btn btn-primary">保存</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript" src="/admin/static/js/plugins/wangEditor/wangEditor.min.js"></script>

<script type="text/javascript">

    //实例化编辑器
    var E = window.wangEditor;
    var editor = new E('#editor');
    editor.customConfig.uploadImgServer = '/admin/system/upload';
    editor.customConfig.uploadImgTimeout = 3000;
    editor.create();
    editor.txt.html('${bean.mailTpl!}');




    function editSave(){
        $("#mailTpl").val(editor.txt.html());
        $.ajax({
            url: '/admin/system/websetSave',
            type: 'post',
            dataType: 'text',
            data: $("#editForm").serialize(),
            success: function (data) {
                refresh();
                $("#lgModal").modal('hide');
                alertMsg("更新成功","success");
            }
        });
    }

</script>