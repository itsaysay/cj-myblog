<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>模板管理</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/index">首页</a>
            </li>
            <li>
                <a href="#">系统设置</a>
            </li>
            <li class="active">
                <strong>模板管理</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <#list list as item>
        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-content product-box">

                    <div class="product-imitation">
                        <img src="${item.scree!}" class="tpl-screen">
                    </div>
                    <div class="product-desc">
                        <a href="${item.url!}" class="product-name" target="_blank">${item.name!}</a>
                        <small class="text-muted">作者：${item.author!}</small><br/>
                        <small class="text-muted">发布时间：${item.pubdate!}</small><br/>
                        <small class="text-muted">最新时间：${item.modified!}</small><br/>
                        <small class="text-muted">版本：${item.version!}</small><br/>
                        <div class="small m-t-xs">
                           ${item.note!}
                        </div>
                        <div class="m-t text-righ">
                            <#if item.id! == theme>

                            <a href="javascript:;" class="btn btn-xs btn-outline btn-warning">当前主题</a>
                            </#if>
                            <a href="/admin/system/tpl_save?theme=${item.id!}" callback="refresh()" data-body="确认设置" target="ajaxTodo" class="btn btn-xs btn-outline btn-primary">设置 <i class="fa fa-long-arrow-right"></i> </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </#list>
    </div>

</div>
