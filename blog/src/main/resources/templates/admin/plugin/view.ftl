<div class="row">
    <div class="col-md-12">
            <div class="box-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="plug-image">
                                    <img src="${bean.image!}" class="tpl-screen">
                                </div>
                                <div class="plug-about">
                                    <a href="${bean.url!}" class="product-name" target="_blank">${bean.name!}</a>
                                    <small class="text-muted">作者：${bean.author!}</small><br/>
                                    <small class="text-muted">发布时间：${bean.pubTime}</small><br/>
                                    <small class="text-muted">修改时间：${bean.modifyTime}</small><br/>
                                    <small class="text-muted">版本：${bean.version!}</small><br/>
                                    <div class="small m-t-xs">
                                    ${bean.description!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="progress progress-striped active" id="progress" style="display:none">
                                    <div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                <button type="button"  onclick="install()" class="btn btn-primary" data-style="expand-right">安装</button>
            </div>
    </div>
</div>
<script type="text/javascript">
    function install(){
        $.ajax({
            url: '/admin/plugin/install',
            type: 'post',
            dataType: 'json',
            data: {skuNumber:"${bean.skuNumber!}"},
            success: function (result) {
                if (result.code == 0 ){
                    $("#progress").css("display","block");
                    setTimeout(function () {
                        listReload();
                        alertMsg("安装成功","success");
                        $("#progress").css("display","none");
                    },500);
                }
            }
        });
    }

</script>