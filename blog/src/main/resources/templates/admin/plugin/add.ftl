<div class="row">
    <div class="col-md-12">
        <form id="addForm" class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-2 control-label"><label id="nameLabel">插件名称*</label></div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <div class="col-sm-2 control-label"><label id="skuNumberLabel">插件唯一标识*</label></div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="skuNumber" id="skuNumber" placeholder="输入英文">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <div class="col-sm-2 control-label"><label>插件作者</label></div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="author" id="author" value="">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <div class="col-sm-2 control-label"><label>插件版本</label></div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="version" id="version" value="">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <div class="col-sm-2 control-label"><label>插件类型</label></div>
                    <div class="col-sm-10">
                        <select name="isMain" class="form-control select2" style="width: 100%;">
                            <option  value="1">前端插件(有后台管理)</option>
                            <option  value="2">前端插件(无后台管理)</option>
                            <option  value="3">后台插件(有后台管理)</option>
                            <option  value="4">后台插件(无后台管理)</option>
                        </select>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <div class="col-sm-2 control-label"><label>插件描述</label></div>
                    <div class="col-sm-10">
                        <textarea class="form-control  m-t Input_text" rows="3" name="description" id="description"></textarea>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

            </div>
            <div class="modal-footer" style="margin-top: 30px;">
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                <button type="button" onclick="pluginSave()" class="btn btn-primary">保存</button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">

    function pluginSave(){
        $("span").remove(".errorClass");
        $("br").remove(".errorClass");

        if ($.trim($("#name").val()) == '' ){
            $("#nameLabel").prepend('<span class="errorClass" style="color:red">该项必填</span><br class="errorClass"/>');
            return false;
        }

        if ($.trim($("#skuNumber").val()) == '' ){
            $("#skuNumberLabel").prepend('<span class="errorClass" style="color:red">该项必填</span><br class="errorClass"/>');
            return false;
        }



        $.ajax({
            url: '/admin/plugin/save',
            type: 'post',
            dataType: 'text',
            data: $("#addForm").serialize(),
            success: function (data) {
                listReload();
                $("#lgModal").modal('hide');
                alertMsg("添加成功","success");
            }
        });
    }

</script>