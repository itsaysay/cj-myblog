<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>插件管理</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/index">首页</a>
            </li>
            <li class="active">
                <strong>插件管理</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>所有插件</h5>
                    <div class="ibox-tools">
                        <#if devMode == 1>
                        <a target="modal" modal="lg" href="/admin/plugin/add" class="btn btn-primary btn-xs">新增插件</a>
                        </#if>
                    </div>
                </div>
                <div class="ibox-content">
                    <!--搜索-->
                    <#--<div class="row">
                        <div class="col-sm-3 m-b-xs">
                            <select class="input-sm form-control input-s-sm inline">
                            <option value="0">Option 1</option>
                            <option value="1">Option 2</option>
                            <option value="2">Option 3</option>
                            <option value="3">Option 4</option>
                        </select>
                        </div>

                        <div class="col-sm-3">
                                <input type="text" placeholder="Search" class="input-sm form-control">
                        </div>

                        <div class="col-sm-6">
                            <button type="button" class="btn btn-sm btn-primary"> Go!</button>
                        </div>

                    </div>-->

                    <!--表格列表-->
                    <div class="table-responsive">
                        <table class="table table-striped" id="tb_list">
                            <thead>
                            <tr>
                                <th>插件名称</th>
                                <th>唯一标示</th>
                                <th>作者</th>
                                <th>状态</th>
                                <th>版本</th>
                                <th>有后台?</th>
                                <th>安装时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var tb_list;
    $(function () {


        tb_list = $('#tb_list').DataTable({
            "processing":true,
            "searching":false,
            "paging":true,
            "bServerSide":true,
            "bInfo":false,
            "bLengthChange":false,
            "bSort":false,
            "language":{"url":"/admin/static/js/plugins/jQdatatables/language.json"},
            "ajax":{"url":"/admin/plugin/page","type":"post"},
            "columns":[
                {"data":"name"},
                {"data":"skuNumber"},
                {"data":"author"},
                {"data":"status"},
                {"data":"version"},
                {"data":"isMain"},
                {"data":"createTime"},
                {"data":null}
            ],
            "columnDefs" : [
                {
                    "targets" : 3,
                    "data" : null,
                    "render" : function(data) {
                       if (data == 1){
                           return "启用";
                       }else if (data == 0){
                           return "停用";
                       }else{
                           return "未安装";
                       }
                    }
                },
                {
                    "targets" : -3,
                    "data" : null,
                    "render" : function(data) {
                        if (data == 1){
                            return "有";
                        }else if (data == 0){
                            return "无";
                        }
                    }
                },
                {
                    "targets" : -1,
                    "data" : null,
                    "render" : function(data) {
                        var statusText = "";
                        var pluText = "卸载";
                        var plurl = "/admin/plugin/uninstall?id="+data.id+"&skuNumber="+data.skuNumber;
                        var status = 0;
                        var btn = "";

                        if (data.status == 1){
                            statusText = "停用";
                            status = 0;
                        }else if (data.status == 0){
                            statusText = "启用";
                            status = 1;
                        }else {
                            pluText = "安装";
                            plurl = "/admin/plugin/view";
                        }

                        if(data.status != 3){
                            btn += '<a  class="btn btn-xs btn-success" callback="listReload();" data-body="确认要'+statusText+'吗？"  target="ajaxTodo"  href="/admin/plugin/edit?id='+data.id+'&status='+status+'">'+statusText+'</a>&nbsp;';
                            btn += '<a class="btn btn-xs btn-danger" callback="listReload();" data-body="确认要'+pluText+'吗？" target="ajaxTodo" href="'+plurl+'">'+pluText+'</a>&nbsp;';
                            btn += '<a class="btn btn-xs btn-success" onclick="pluginExport('+data.id+')">导出</a>';

                        }else {
                            btn += '<a  class="btn btn-xs btn-success" target="modal" modal="lg" href="'+plurl+'?skuNumber='+data.skuNumber+'">'+pluText+'</a>&nbsp;';
                        }

                        return btn;
                    }
                }
            ]
        }).on('xhr.dt', function ( e, settings, data ,xhr) {
            dataTableMsg(data,xhr);
        } );

    });

    function listReload() {
        var param = {
        };
        tb_list.settings()[0].ajax.data = param;
        tb_list.ajax.reload();
    }

    function pluginExport(id) {
        $.GET("/admin/plugin/export",{id:id},function (reslult) {
            if (reslult.code == 0){
                window.location = result.msg;
            }else {
                alertMsg(reslult.msg);
            }
        })
    }

</script>