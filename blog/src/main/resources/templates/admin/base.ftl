<#macro style>

<link href="/admin/static/css/bootstrap.min.css" rel="stylesheet">
<link href="/admin/static/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/admin/static/css/animate.css" rel="stylesheet">
<link href="/admin/static/css/style.css" rel="stylesheet">
<link href="/admin/static/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<!-- FooTable -->
<link href="/admin/static/css/plugins/footable/footable.core.css" rel="stylesheet">

<!--Data picker-->
<link href="/admin/static/css/plugins/datepicker/datepicker3.css" rel="stylesheet">

<link href="/admin/static/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<!--face-->
<link href="/admin/static/css/face.css" rel="stylesheet">

</#macro>

<#macro js>

<!--main-->
<script src="/admin/static/js/jquery-2.1.1.js"></script>
<script src="/admin/static/js/bootstrap.min.js"></script>
<script src="/admin/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/admin/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/admin/static/js/app.js"></script>

<!-- Flot -->
<script src="/admin/static/js/plugins/flot/jquery.flot.js"></script>
<script src="/admin/static/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="/admin/static/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="/admin/static/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="/admin/static/js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="/admin/static/js/plugins/peity/jquery.peity.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="/admin/static/js/inspinia.js"></script>
<script src="/admin/static/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="/admin/static/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- GITTER -->
<script src="/admin/static/js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="/admin/static/js/plugins/sparkline/jquery.sparkline.min.js"></script>


<!-- ChartJS-->
<script src="/admin/static/js/plugins/chartJs/Chart.min.js"></script>

<!-- Toastr -->
<script src="/admin/static/js/plugins/toastr/toastr.min.js"></script>

<!-- FooTable -->
<script src="/admin/static/js/plugins/footable/footable.all.min.js"></script>

<!-- DataTables -->
<script src="/admin/static/js/plugins/jQdatatables/jquery.dataTables.min.js"></script>
<script src="/admin/static/js/plugins/jQdatatables/dataTables.bootstrap.min.js"></script>

<!-- Data picker -->
<script src="/admin/static/js/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/admin/static/js/plugins/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>

<!--tagsinput-->
<script src="/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.min.js"></script>
<script src="/admin/static/js/plugins/tagsinput/typeahead.bundle.min.js"></script>
<!--face-->
<script src="/admin/static/js/face.js"></script>
</#macro>


<#macro menu>
<ul class="nav metismenu" id="side-menu">
    <li class="nav-header">
        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="/admin/static/img/profile_small.jpg" />
                             </span>
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">admin</strong>
                             </span> <span class="text-muted text-xs block">管理员 <b class="caret"></b></span> </span> </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a href="#">个人信息</a></li>
                <li><a href="#">联系方式</a></li>
                <li><a href="#">邮箱</a></li>
                <li class="divider"></li>
                <li><a href="/admin/logout">退出登录</a></li>
            </ul>
        </div>
        <div class="logo-element">
            IN+
        </div>
    </li>


    <li>
        <a href="javascript:;" href-url="/admin/part/list"><i class="fa fa-bars"></i> <span class="nav-label">栏目管理</span></a>
    </li>
    <li>
        <a href="javascript:;" href-url="/admin/article/list"><i class="fa fa-book"></i> <span class="nav-label">文章管理</span></a>
    </li>
    <li>
        <a href="javascript:;" href-url="/admin/comment/list"><i class="fa fa-comment"></i> <span class="nav-label">评论管理</span></a>
    </li>
    <li>
        <a href="javascript:;" href-url="/admin/plugin/list"><i class="fa fa-plug"></i> <span class="nav-label">插件管理</span></a>
    </li>
    <li>
        <a href="#"><i class="fa fa-wrench"></i> <span class="nav-label">系统设置</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li class="active"><a href="javascript:;" href-url="/admin/system/tpl_list">模板设置</a></li>
            <li class="active"><a href="javascript:;" href-url="/admin/system/webset">网站设置</a></li>

        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-plug"></i> <span class="nav-label">插件</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <#list pluginList as plugin>
            <li class="active"><a href="javascript:;" href-url="/plugins/admin/${plugin.skuNumber!}/index">${plugin.skuNumber!}</a></li>
            </#list>
        </ul>
    </li>

</ul>
</#macro>


<#macro head>
<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <li>
            <span class="m-r-sm text-muted welcome-message">欢迎登录管理后台</span>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
            </a>
            <ul class="dropdown-menu dropdown-messages">
                <li>
                    <div class="dropdown-messages-box">
                        <a href="profile.html" class="pull-left">
                            <img alt="image" class="img-circle" src="/admin/static/img/a7.jpg">
                        </a>
                        <div class="media-body">
                            <small class="pull-right">46小时前</small>
                            <strong>李文俊</strong> 关注了 <strong>刘海洋</strong>. <br>
                            <small class="text-muted">3 天 前- 10.06.2014</small>
                        </div>
                    </div>
                </li>
                <li class="divider"></li>
                <li>
                    <div class="dropdown-messages-box">
                        <a href="profile.html" class="pull-left">
                            <img alt="image" class="img-circle" src="/admin/static/img/a4.jpg">
                        </a>
                        <div class="media-body ">
                            <small class="pull-right text-navy">5小时前</small>
                            <strong>王昆</strong> 关注了 <strong>李文俊</strong>. <br>
                            <small class="text-muted">昨天下午1:21 - 11.06.2014</small>
                        </div>
                    </div>
                </li>
                <li class="divider"></li>
                <li>
                    <div class="dropdown-messages-box">
                        <a href="profile.html" class="pull-left">
                            <img alt="image" class="img-circle" src="/admin/static/img/profile.jpg">
                        </a>
                        <div class="media-body ">
                            <small class="pull-right">23小时前</small>
                            <strong>张三</strong> 赞了 <strong>李四</strong>. <br>
                            <small class="text-muted">2天前 - 11.06.2014</small>
                        </div>
                    </div>
                </li>
                <li class="divider"></li>
                <li>
                    <div class="text-center link-block">
                        <a href="mailbox.html">
                            <i class="fa fa-envelope"></i> <strong>查看更多消息</strong>
                        </a>
                    </div>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
            </a>
            <ul class="dropdown-menu dropdown-alerts">
                <li>
                    <a href="mailbox.html">
                        <div>
                            <i class="fa fa-envelope fa-fw"></i> 您有 16 条未读通知
                            <span class="pull-right text-muted small">4 分钟 前</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="profile.html">
                        <div>
                            <i class="fa fa-twitter fa-fw"></i> 3 个新粉丝
                            <span class="pull-right text-muted small">12 分钟 前</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="grid_options.html">
                        <div>
                            <i class="fa fa-upload fa-fw"></i> 服务器重启
                            <span class="pull-right text-muted small">4 分钟 前</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <div class="text-center link-block">
                        <a href="notifications.html">
                            <strong>查看更多通知</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </li>
            </ul>
        </li>


        <li>
            <a href="/admin/logout">
                <i class="fa fa-sign-out"></i> 退出登录
            </a>
        </li>
    </ul>

</nav>
</#macro>


<#macro chat>
    <div class="small-chat-box fadeInRight animated">

        <div class="heading" draggable="true">
            <small class="chat-date pull-right">
                02.19.2015
            </small>
            Small chat
        </div>

        <div class="content">

            <div class="left">
                <div class="author-name">
                    Monica Jackson <small class="chat-date">
                    10:02 am
                </small>
                </div>
                <div class="chat-message active">
                    Lorem Ipsum is simply dummy text input.
                </div>

            </div>
            <div class="right">
                <div class="author-name">
                    Mick Smith
                    <small class="chat-date">
                        11:24 am
                    </small>
                </div>
                <div class="chat-message">
                    Lorem Ipsum is simpl.
                </div>
            </div>
            <div class="left">
                <div class="author-name">
                    Alice Novak
                    <small class="chat-date">
                        08:45 pm
                    </small>
                </div>
                <div class="chat-message active">
                    Check this stock char.
                </div>
            </div>
            <div class="right">
                <div class="author-name">
                    Anna Lamson
                    <small class="chat-date">
                        11:24 am
                    </small>
                </div>
                <div class="chat-message">
                    The standard chunk of Lorem Ipsum
                </div>
            </div>
            <div class="left">
                <div class="author-name">
                    Mick Lane
                    <small class="chat-date">
                        08:45 pm
                    </small>
                </div>
                <div class="chat-message active">
                    I belive that. Lorem Ipsum is simply dummy text.
                </div>
            </div>


        </div>
        <div class="form-chat">
            <div class="input-group input-group-sm"><input type="text" class="form-control"> <span class="input-group-btn"> <button
                    class="btn btn-primary" type="button">Send
                    </button> </span></div>
        </div>

    </div>
    <div id="small-chat">

        <span class="badge badge-warning pull-right">5</span>
        <a class="open-small-chat">
            <i class="fa fa-comments"></i>

        </a>
    </div>

<!--加载动画-->
<div id="loading" class="loading-panel">
    <div class="sk-spinner sk-spinner-circle">
        <div class="sk-circle1 sk-circle"></div>
        <div class="sk-circle2 sk-circle"></div>
        <div class="sk-circle3 sk-circle"></div>
        <div class="sk-circle4 sk-circle"></div>
        <div class="sk-circle5 sk-circle"></div>
        <div class="sk-circle6 sk-circle"></div>
        <div class="sk-circle7 sk-circle"></div>
        <div class="sk-circle8 sk-circle"></div>
        <div class="sk-circle9 sk-circle"></div>
        <div class="sk-circle10 sk-circle"></div>
        <div class="sk-circle11 sk-circle"></div>
        <div class="sk-circle12 sk-circle"></div>
    </div>
</div>
<!--弹出框-->
<div class="modal fade" id="lgModal">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        </div>
        <div class="modal-body">
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="smModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                </button>
                <h4 class="modal-title">提示</h4>
            </div>
            <div class="modal-body">
                <p>确认删除？</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary">确认</button>
            </div>
        </div>
    </div>
</div>

<script>
    //ajax table 返回信息
    function dataTableMsg(data,xhr) {
        if (xhr.status == 200){
        }else {
        }
    }
</script>

</#macro>


<#macro footer>
    <div>
        <strong>Copyright</strong> IN+ &copy; 2017-2099
    </div>
</#macro>

