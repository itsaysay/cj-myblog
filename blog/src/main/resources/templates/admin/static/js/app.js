/**
 * Created by jujunchen on 2017/8/31.
 */

var currentPageUrl = "";

/**
 * 重新载入当前页
 */
function refresh() {
    $("#loading").show();
    $.ajax({
        url:currentPageUrl,
        dataType:"html",
        success:function(result){
            $("#loading").hide();
            $("#main-content").html(result);
        },
        error:function(err){
            $("#loading").hide();
            console.log(err)
        }
    })
}


/**
 *  checkbox 全选
 */

$(document).on("click","#checkAll",function () {
    var $this = $(this);
    var flag = eval( $this.attr("flag"));
    if (flag){
        $("#tb_list :checkbox").prop("checked", false);
        $this.attr("flag","false");
    }else {
        $("#tb_list :checkbox").prop("checked", true);
        $this.attr("flag","true");
    }
});


/**
 * 点击主菜单，获取页面内容
 */
$("#side-menu").on("click","a[href-url]",function(){
    var that = $(this);
    var _text = that.text();
    var _href = that.attr("href-url");

    currentPageUrl = _href;

    $("#loading").show();
    $.ajax({
        url:_href,
        dataType:"html",
        success:function(result){
            $("#loading").hide();
            $("#main-content").html(result);
        },
        error:function(err){
            $("#loading").hide();
            console.log(err)
        }
    })
    return false;
});


/**
 * 提示信息
 * @param message
 * @param title
 * @param type
 * @param position
 * @param timOut
 */
function alertMsg(message,type,title,position,timOut) {

    var positionClass = "toast-top-center";

    if (position != undefined){
        positionClass = "toast-"+position;
    }

    if (timOut == undefined){
        timOut = "3000";
    }

    if (message == undefined){
        message = "";
    }

    if (title == undefined){
        title = "";
    }

    if (type == undefined){
        type = "info";
    }

    toastr.options = {
        closeButton: true,
        debug: false,
        progressBar: false,
        preventDuplicates: false,
        positionClass: positionClass,
        onclick: null,
        showDuration: "400",
        hideDuration: "1000",
        timeOut: timOut,
        extendedTimeOut: "1000",
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
    };

   toastr[type](message, title);

}


$(document).on("click","a[target='modal']",function(){
    var that = $(this);
    var href = that.attr("href");
    var method = that.attr("method") == "post"?"post":"get";
    var modal = that.attr("modal")
    $("#loading").show();
    $.ajax({
        url:href,
        type:method,
        dataType:"html",
        success:function(result){
            $("#loading").hide();
            $("#lgModal").find(".modal-dialog").attr("class","modal-dialog modal-"+modal);
            $("#lgModal").find(".modal-body").html(result).end().modal("show");
        },
        error:function(){
            $("#loading").hide();
            console.error("加载失败");
        }
    })
    return false;
});

// 确认对话框
$(document).on("click","a[target='ajaxTodo']",function(){
    var that = $(this);
    var href = that.attr("href");
    var title = that.attr("data-body");
    var cb = that.attr("callback");
    $("#smModal").attr("action",href).attr("callback",cb).find(".modal-body").html(title).end().modal("show");

    return false;
});

$("#smModal").on("click",".modal-footer>.btn-primary",function(){
    var action = $("#smModal").attr("action");
    var clcb = $("#smModal").attr("callback");
    if (!action) {
        $("#smModal").modal("hide");
        return false;
    }

    $.get(action,function(){
        $("#smModal").modal("hide");
        if (!!clcb) {
            // console.log(clcb);
            eval(clcb);
        }else{
            alertMsg("操作成功","success");
        }
    })

});




