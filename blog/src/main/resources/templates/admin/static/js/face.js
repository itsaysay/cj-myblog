/**
 * Created by jujunchen on 2017/10/14.
 */
var ImgIputHandler={
    themePath:"",
    Init:function(){
        var isShowImg=false;
        $(".imgBtn").click(function(){
            if(isShowImg==false){
                isShowImg=true;
                $(this).parent().prev().animate({marginTop:"-121px"},200);
                if($(".faceDiv").children().length==0){
                    var  faceJson = ImgIputHandler.themePath + "/static/js/face.json";

                    $.ajax({
                        url:faceJson,
                        dataType:'json',
                        async:false,
                        success:function (json) {
                            for(var i=0;i<json.length;i++){
                                $(".faceDiv").append("<img title=\""+json[i].faceName+"\" src="+ImgIputHandler.themePath+json[i].facePath+"\>");
                            }
                        }
                    });

                    $(".faceDiv>img").click(function(){

                        isShowImg=false;
                        $(this).parent().animate({marginTop:"2px"},200);
                        ImgIputHandler.insertAtCursor($(".Input_text")[0],"["+$(this).attr("title")+"]");
                    });
                }
            }else{
                isShowImg=false;
                $(this).parent().prev().animate({marginTop:"2px"},200);
            }
        });
    },
    insertAtCursor:function(myField, myValue) {
        if (document.selection) {
            myField.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            sel.select();
        } else if (myField.selectionStart || myField.selectionStart == "0") {
            var startPos = myField.selectionStart;
            var endPos = myField.selectionEnd;
            var restoreTop = myField.scrollTop;
            myField.value = myField.value.substring(0, startPos) + myValue + myField.value.substring(endPos, myField.value.length);
            if (restoreTop > 0) {
                myField.scrollTop = restoreTop;
            }
            myField.focus();
            myField.selectionStart = startPos + myValue.length;
            myField.selectionEnd = startPos + myValue.length;
        } else {
            myField.value += myValue;
            myField.focus();
        }
    }
}