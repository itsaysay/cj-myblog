<#include "base.ftl" />
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>登录</title>

<@style/>

</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">IN+</h1>

        </div>
        <form class="m-t" role="form" id="loginForm">
            <div class="form-group">
                <input type="text" class="form-control" name="username" placeholder="用户名" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="密码" required="">
            </div>
            <button type="submit" id="subForm" class="btn btn-primary block full-width m-b">登录</button>

            <a href="login.html#"><small>忘记密码?</small></a>
        </form>
    </div>
</div>

<!-- Mainly scripts -->
<@js/>
<script>
    /*$(document).keydown(function (event) {
        if (event.keyCode == 13){
            $.post("/admin/login",$("#loginForm").serialize(),function (result) {
                if (result.code != 0){
                    alertMsg(result.msg);
                }else {
                    window.location.href = "/admin/index" ;
                }
            })
        }
    });*/


    $("#loginForm").submit(function () {
        $.post("/admin/login",$("#loginForm").serialize(),function (result) {
            if (result.code != 0){
                alertMsg(result.msg);
                return false;
            }else {
                window.location.href = "/admin/index" ;
            }
        })
        return false
    });

</script>


</body>

</html>
