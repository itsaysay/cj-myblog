
<link href="/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

<style>
    .bootstrap-tagsinput{
        width: 100%;
        border: 1px solid #e5e6e7;
        border-radius: 1px;
    }

</style>


<div class="box">
    <div class="ibox-content">

        <form class="form-horizontal" id="addForm">
            <div class="form-group">
                <label class="col-sm-2 control-label">标题</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="title" id="title" value=""></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">作者</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="anchor" id="anchor" value="${userName!}"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">栏目</label>
                <div class="col-sm-10">
                    <select name="partId" id="partId" class="form-control select2" style="width: 100%;">
                    <#list partList as list>
                        <option  value="${list.id}" >${list.name}</option>
                    </#list>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">标签</label>
                <div class="col-sm-10"><input type="text" class="form-control" id="tags" name="tags" value="" data-role="tagsinput" ></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">状态</label>
                <div class="col-sm-10">
                    <select name="status" id="status" class="form-control select2" style="width: 100%;">
                        <option  value="1" >启用</option>
                        <option  value="0" >禁用</option>
                        <option  value="2" >置顶</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">属性</label>
                <div class="col-sm-10">
                    <div class="checkbox checkbox-primary">
                        <input name="isDirectory"  id="isDirectory" type="checkbox" value="1"><label>生成文章目录</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div id="editor" style="height: 350px"></div>
                </div>
            </div>
            <div class="form-group" style="height:15px;">
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label class="pull-right control-label"><button type="button" class="btn btn-info btn-xs" onclick="getSubject(200)">复制正文200字</button></label>
                </div>
                <div class="col-sm-12">
                    <div id="editor2" style="height: 200px"></div>
                </div>
            </div>
            <div class="form-group" style="height:15px;">
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-sm-2 control-label">SEO标题</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="seoTitle" id="seoTitle" value=""></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">SEO关键词</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="seoKeywords" id="seoKeywords" value=""></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">SEO描述</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="seoDescriptions" id="seoDescriptions" value=""></div>
            </div>

            <div class="mail-body text-right tooltip-demo">
                <button type="button" class="btn btn-white" onclick="getArticleList()">关闭</button>
                <button type="button" onclick="partSave()" class="btn btn-primary">保存</button>
            </div>
        </form>

        <script type="text/javascript" src="/admin/static/js/plugins/wangEditor/wangEditor.min.js"></script>
        <script type="text/javascript">
            //实例化编辑器
            var E = window.wangEditor
            var editor = new E('#editor')
            editor.customConfig.uploadImgServer = '/admin/system/upload';
            editor.customConfig.uploadImgTimeout = 3000;
            editor.create()
            editor.txt.html();

            var editor2 = new E('#editor2');
            editor2.create();
            editor2.txt.html();

            //标签，bootstrap-tagsinput
            var tags = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: {
                    url: '/admin/tag/list',
                    filter: function(list) {
                        return $.map(list, function(data) {
                            return { id: data.id,name:data.tag };
                        });
                    }
                }
            });
            tags.initialize();

            $('#tags').tagsinput({
                typeaheadjs: {
                    name: 'tag',
                    displayKey: 'name',
                    valueKey: 'name',
                    source: tags.ttAdapter()
                }
            });

            function partSave(){

                if ($.trim(editor2.txt.text()) ==""){
                    getSubject(200);
                }

                var params = {
                    "id":$("#id").val(),
                    "title":$("#title").val(),
                    "anchor":$("#anchor").val(),
                    "partId":$("#partId").val(),
                    "tags":$("#tags").val(),
                    "status":$("#status").val(),
                    "content":editor.txt.html(),
                    "subject":editor2.txt.html(),
                    "seoTitle":$("#seoTitle").val(),
                    "seoKeywords":$("#seoKeywords").val(),
                    "seoDescriptions":$("#seoDescriptions").val(),
                    "isDirectory":$("#isDirectory").is(':checked')?$("#isDirectory").val():0
                };
                $.ajax({
                    url: '/admin/article/save',
                    type: 'post',
                    dataType: 'text',
                    data: params,
                    success: function (data) {
                        getArticleList();
                        $("#lgModal").modal('hide');
                        alertMsg("新增成功","success");
                    }
                });

            }


            function getSubject(length) {
                var content = editor.txt.text();
                var subject = content.substr(0,length);
                editor2.txt.text(subject);
            }

        </script>
    </div>
</div>
