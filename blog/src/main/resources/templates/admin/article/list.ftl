<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>文章管理</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin/index">首页</a>
            </li>
            <li class="active">
                <strong>文章管理</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <a class="btn btn-block btn-primary compose-mail" href="javascript:;" id="writeArticle" href-url="/admin/article/add">写文章</a>
                        <div class="space-25"></div>
                        <h5>栏目</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <#list list as item>
                            <li><a href="javascript:;" onclick="getArticleList(${item.id},'')">${item.name!} <span class="label label-warning pull-right">${item.count!}</span> </a></li>
                            </#list>
                        </ul>


                        <h5 class="tag-title">标签</h5>
                        <ul class="tag-list" style="padding: 0">
                            <#list tags as item>
                            <li><a href="javascript:;" onclick="getArticleList('','${item.tag}')"><i class="fa fa-tag"></i> ${item.tag}</a></li>
                            </#list>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 animated fadeInRight" id="artContent">

        </div>
    </div>
</div>
<script>
    var article_list;
    $(function () {

        article_list = function (params) {
            $("#loading").show();
            $.ajax({
                url:"/admin/article/articleList",
                data:params,
                dataType:"html",
                success:function(result){
                    $("#loading").hide();
                    $("#artContent").html(result);
                },
                error:function(err){
                    $("#loading").hide();
                    console.log(err)
                }
            })
        }

        article_list({});


        //载入新增文章页面
        $("#writeArticle").on("click",function () {
            var that = $(this);
            var _href = that.attr("href-url");

            $("#loading").show();
            $.ajax({
                url:_href,
                dataType:"html",
                success:function(result){
                    $("#loading").hide();
                    $("#artContent").html(result);
                },
                error:function(err){
                    $("#loading").hide();
                    console.log(err)
                }
            })
            return false;
        })

    })
    
    function getArticleList(partId,tags) {
        var params={
            "partId":partId = undefined?"":partId,
            "tags":tags = undefined?"":tags
        }
        article_list(params);
    }

</script>