    <div class="mail-box-header">
        <div class="row">
            <div class="col-sm-8 m-b-xs">
                <div class="input-daterange input-group" id="datepicker">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" class="input-sm form-control" name="beginTime" id="beginTime" value=""/>
                    <span class="input-group-addon">到</span>
                    <input type="text" class="input-sm form-control" name="endTime" id="endTime" value="" />
                </div>
            </div>
            <div class="col-sm-4 m-b-xs">
                <div class="input-group">
                    <input type="text" class="form-control input-sm" name="title" id="title" placeholder="输入标题">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-primary" onclick="listReload()">
                            搜索
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="mail-tools tooltip-demo m-t-md">
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" id="checkAll" flag="false"><i class="fa fa-check-square-o"></i> 全选</button>
                <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" onclick="deleteArt()"><i class="fa fa-trash-o"></i> 删除</button>
            </div>
        </div>
    </div>
    <div class="mail-box">
        <table class="table table-hover table-mail" id="tb_list">
            <thead>
            <tr>
                <th></th>
                <th class="tb-title">标题</th>
                <th>作者</th>
                <th>点击量</th>
                <th>状态</th>
                <th>发布时间</th>
            </tr>
            </thead>
        </table>
    </div>
<script>
    var tb_list;
    var partId = "${partId!}";
    var tags = "${tags!}"
    $(function () {
        //初始化时间选择器
        $('.input-daterange').datepicker({
            language: 'zh-CN',
            format: 'yyyy-mm-dd',
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });


        tb_list = $('#tb_list').DataTable({
            "dom":'itflp',
            "processing":true,
            "searching":false,
            "paging":true,
            "bServerSide":true,
            "bInfo":false,
            "bLengthChange":false,
            "bSort":false,
            "language":{"url":"/admin/static/js/plugins/jQdatatables/language.json"},
            "ajax":{"url":"/admin/article/page","type":"post","data":{"partId":partId,"tags":tags}},
            "columns":[
                {"data":null},
                {"data":null},
                {"data":"anchor"},
                {"data":"hits"},
                {"data":"status"},
                {"data":"createTime"}
            ],
            "columnDefs" : [
                {
                    "targets" : 0,
                    "data" : null,
                    "render" : function(data) {
                        var icheck = '<div class="checkbox checkbox-primary">'
                                     +'<input name="ids" type="checkbox" value="'+data.id+'"><label></label></div>';
                        return icheck;
                    }
                },
                {
                    "targets" : 1,
                    "data" : null,
                    "render" : function(data) {
                        var ititle = '<a class="art"  href="javascript:;" href-url="/admin/article/edit?id='+data.id+'" >'+data.title+'</a>';
                        return ititle;
                    }
                },
                {
                    "targets" : 4,
                    "data" : null,
                    "render" : function(data) {
                        if (data == 1){
                            return "启用";
                        }else if (data == 0){
                            return "禁用";
                        }else if (data == 2){
                            return "置顶";
                        }
                    }
                }

            ]
        }).on('preXhr.dt', function ( e, settings, data ) {
        } );

        //文章编辑
        $("#tb_list").on("click",".art",function () {
            var that = $(this);
            var _href = that.attr("href-url");

            $("#loading").show();
            $.ajax({
                url:_href,
                dataType:"html",
                success:function(result){
                    $("#loading").hide();
                    $("#artContent").html(result);
                },
                error:function(err){
                    $("#loading").hide();
                    console.log(err)
                }
            })
            return false;
        })


    })


    //搜索，列表重载
    function listReload() {
        var param = {
            "beginTime":$("#beginTime").val(),
            "endTime":$("#endTime").val(),
            "title":$("#title").val()
        };
        tb_list.settings()[0].ajax.data = param;
        tb_list.ajax.reload();
    }

/*    //标签，列表重载
    function listReloadByTag(tag) {
        var param = {
            "tags":tag
        }
        tb_list.settings()[0].ajax.data = param;
        tb_list.ajax.reload();
    }*/

    //删除
    function deleteArt() {
        var ids = "";
        $("input[name='ids']:checked").each(function () {
            ids += $(this).val()+","
        })
        if(ids.length ==0){
            alertMsg("请选择项目","warning");
            return false;
        }
        ids = ids.substr(0,ids.length-1);
       $.post("/admin/article/delete","ids="+ids,function () {
            listReload();
       })
    }


</script>