package com.cj.myblog.controller.admin;

import com.cj.myblog.controller.BaseController;
import com.cj.myblog.model.*;
import com.cj.myblog.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

import static com.cj.myblog.constants.Constants.*;


/**
 * Created by jujunchen on 2017/8/29.
 */
@Controller
@RequestMapping("/admin/comment/")
public class CommentController extends BaseController{

    @Autowired
    private CommentService commentService;

    @GetMapping(LIST)
    public void list(){
    }

    @ResponseBody
    @PostMapping(PAGE)
    public PageBean<CommentDo> page(CommentDo commentDo){
        PageBean<CommentDo> list = commentService.findAll(commentDo);
        return list;
    }

    @ResponseBody
    @GetMapping(DELETE)
    public void delete(Long id){
        commentService.delete(id);
    }

    @GetMapping(EDIT)
    public void edit(ModelMap modelMap,Long id){
        CommentDo commentDo = commentService.findOneById(id);
        modelMap.put("bean",commentDo);
        //modelMap.put("themePath","/theme/"+ Constants.options.get("theme"));
    }

    @ResponseBody
    @PostMapping(SAVE)
    public void save(CommentDo commentDo){
        UserDo userDo = currentUser();
        commentDo.setName(userDo.getUsername());
        commentDo.setCreateTime(new Date());
        commentService.save(commentDo);
    }



}
