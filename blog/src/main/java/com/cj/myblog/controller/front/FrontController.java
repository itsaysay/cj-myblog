package com.cj.myblog.controller.front;

import com.cj.myblog.constants.ResultEnum;
import com.cj.myblog.common.util.VerifyCodeUtil;
import com.cj.myblog.controller.BaseController;
import com.cj.myblog.model.ArticleDo;
import com.cj.myblog.model.CommentDo;
import com.cj.myblog.model.Result;
import com.cj.myblog.service.ArticleService;
import com.cj.myblog.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by jujunchen on 2017/8/29.
 */
@Controller
@RequestMapping("/")
public class FrontController extends BaseController{

    @Autowired
    private ArticleService articleService;

    @Autowired
    private CommentService commentService;


    @GetMapping(value = {"","index"})
    public ModelAndView noindex(ModelAndView modelAndView,ArticleDo articleDo){
        List<ArticleDo> list = articleService.frontFindAll(articleDo);
        modelAndView.addObject("list",list);
        modelAndView.setViewName("front/index");
        return modelAndView;
    }


    @GetMapping("list/{id}")
    public ModelAndView list(ModelAndView modelAndView,@PathVariable("id")Long id){
        ArticleDo articleDo = new ArticleDo();
        articleDo.setPartId(id);
        List<ArticleDo> list = articleService.frontFindAll(articleDo);
        modelAndView.addObject("list",list);
        modelAndView.setViewName("front/index");
        return  modelAndView;
    }


    @GetMapping("view/{id}")
    public ModelAndView view(ModelAndView modelAndView,@PathVariable("id") Long id){
        ArticleDo articleDo = articleService.frontFindById(id);
        List<CommentDo> commentList = commentService.frontFindByArtId(id);
        articleDo.setCommentCount(commentList.size());
        modelAndView.addObject("commentList",commentList);
        modelAndView.addObject("view",articleDo);
        modelAndView.setViewName("front/view");
        return modelAndView;
    }


    @PostMapping("commentSave")
    @ResponseBody
    public Result commentSave(ModelAndView modelAndView, CommentDo commentDo){
        String verifyCode = commentDo.getVerifyCode();
        HttpSession session = request.getSession();
        String sessionCode = (String) session.getAttribute("verifyCode");
        if (!verifyCode.equalsIgnoreCase(sessionCode)){
            return new Result(ResultEnum.VERIFYCODE_ERROR);
        }
        commentDo.setCreateTime(new Date());
        commentService.save(commentDo);
        return new Result("view/"+commentDo.getArtId());
    }


    @GetMapping("verifyCode")
    public void verifyCode(HttpServletRequest request, HttpServletResponse response){
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");

        //生成随机字串
        String verifyCode = VerifyCodeUtil.generateVerifyCode(4);
        //存入会话session
        HttpSession session = request.getSession(true);
        session.setAttribute("verifyCode", verifyCode.toLowerCase());
        //生成图片
        int w = 200, h = 80;
        try {
            VerifyCodeUtil.outputImage(w, h, response.getOutputStream(), verifyCode);
        } catch (IOException e) {
            logger.error("验证码生成失败");
        }


    }









}
