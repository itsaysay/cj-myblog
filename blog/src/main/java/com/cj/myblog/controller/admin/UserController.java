package com.cj.myblog.controller.admin;

import com.cj.myblog.constants.Constants;
import com.cj.myblog.controller.BaseController;
import com.cj.myblog.model.PluginDo;
import com.cj.myblog.model.Result;
import com.cj.myblog.model.UserDo;
import com.cj.myblog.service.PluginService;
import com.cj.myblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by jujunchen on 2017/8/29.
 */
@Controller
@RequestMapping("/admin/")
public class UserController extends BaseController{

    @Autowired
    private UserService userService;

    @Autowired
    private PluginService pluginService;


    @RequestMapping("index")
    public void index(ModelMap modelMap){
        List<PluginDo> list = pluginService.findAllByStatus();
        modelMap.put("pluginList",list);
    }

    @RequestMapping(value = "login",method = RequestMethod.GET)
    public void login(){

    }

    @ResponseBody
    @RequestMapping(value = "login",method = RequestMethod.POST)
    public Result<UserDo> submitLogin(UserDo userDo){
        Result<UserDo> result = userService.findByUsernameAndPassword(userDo);
        if (result.isSuccess()){
           currentSession().setAttribute(Constants.SESSION_KEY,result.getData());
        }
        return result;
    }

    @RequestMapping(value = "logout",method = RequestMethod.GET)
    public String logout(){
        currentSession().removeAttribute(Constants.SESSION_KEY);
        return redirect("/admin/login");
    }

}
