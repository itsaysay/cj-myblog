package com.cj.myblog.controller;

import com.cj.myblog.constants.Constants;
import com.cj.myblog.model.UserDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.MessageFormat;

/**
 * Created by jujunchen on 2017/8/29.
 */
@Component("baseController")
public class BaseController {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected HttpServletRequest request;


   /* @InitBinder
    protected void initBinder(HttpServletRequest request,
                              ServletRequestDataBinder binder) throws Exception {
        binder.registerCustomEditor(int.class, new IntegerEditor());
        binder.registerCustomEditor(long.class, new LongEditor());
        binder.registerCustomEditor(double.class, new DoubleEditor());
        binder.registerCustomEditor(Date.class, new DateEditor());
    }*/


    protected HttpSession currentSession(){
        return request.getSession();
    }

    protected UserDo currentUser(){
        return (UserDo) request.getSession().getAttribute(Constants.SESSION_KEY);
    }

    public static String redirect(String format, Object... arguments) {
        return new StringBuilder("redirect:").append(MessageFormat.format(format, arguments)).toString();
    }

}
