package com.cj.myblog.controller.admin;

import com.cj.myblog.common.util.CommUtil;
import com.cj.myblog.controller.BaseController;
import com.cj.myblog.model.*;
import com.cj.myblog.service.ArticleService;
import com.cj.myblog.service.PartService;
import com.cj.myblog.service.TagService;
import com.xiaoleilu.hutool.util.BeanUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static com.cj.myblog.constants.Constants.*;


/**
 * Created by jujunchen on 2017/8/29.
 */
@Controller
@RequestMapping("/admin/article/")
public class ArticleController extends BaseController{

    @Autowired
    private ArticleService articleService;

    @Autowired
    private PartService partService;

    @Autowired
    private TagService tagService;

    @RequestMapping(value = LIST,method = RequestMethod.GET)
    public void list(ModelMap modelMap){
        Result<List<PartDo>> result = partService.findAll();
        Result<List<TagDo>> tagResult = tagService.findAll();
        modelMap.put("list",result.getData());
        modelMap.put("tags",tagResult.getData());
    }

    @RequestMapping(value = "articleList",method = RequestMethod.GET)
    public void articleList(ModelMap modelMap,Long partId,String tags){
        modelMap.put("partId",partId);
        modelMap.put("tags",tags);
    }

    @ResponseBody
    @RequestMapping(value = PAGE,method = RequestMethod.POST)
    public PageBean<ArticleDo> page(ArticleDo articleDo){
        PageBean<ArticleDo> list = articleService.findAll(articleDo);
        return list;
    }

    @ResponseBody
    @RequestMapping(value = DELETE,method = RequestMethod.POST)
    public void delete(String ids){
        List<String> strList = StrUtil.split(ids,',');
        List<Long>  longList = CommUtil.stringToLongList(strList);
        articleService.deleteByIdIn(longList);
    }

    @RequestMapping(value = EDIT,method = RequestMethod.GET)
    public void view(ModelMap modelMap,Long id){
        ArticleDo articleDo = articleService.findById(id);
        List<PartDo> partDoList = partService.findAll().getData();
        modelMap.put("bean",articleDo);
        modelMap.put("partList",partDoList);
    }

    @ResponseBody
    @RequestMapping(value = SAVE,method = RequestMethod.POST)
    public Result<String> save(@RequestParam Map<String,Object> map) {
        ArticleDo articleDo = BeanUtil.mapToBean(map,ArticleDo.class,true);
        Result<String> result = articleService.save(articleDo);
        return result;
    }

    @RequestMapping(value = ADD,method = RequestMethod.GET)
    public void add(ModelMap modelMap){
        UserDo userDo = currentUser();
        List<PartDo> partDoList = partService.findAll().getData();
        modelMap.put("partList",partDoList);
        modelMap.put("userName",userDo.getUsername());
    }



}
