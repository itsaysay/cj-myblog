package com.cj.myblog.controller.admin;

import com.cj.myblog.controller.BaseController;
import com.cj.myblog.model.Result;
import com.cj.myblog.model.TagDo;
import com.cj.myblog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static com.cj.myblog.constants.Constants.LIST;


/**
 * Created by jujunchen on 2017/8/29.
 */
@Controller
@RequestMapping("/admin/tag/")
public class TagController extends BaseController{

    @Autowired
    private TagService tagService;

    @ResponseBody
    @RequestMapping(value = LIST,method = RequestMethod.GET)
    public List<TagDo> list(){
        Result<List<TagDo>> tagResult = tagService.findAll();
        return tagResult.getData();
    }



}
