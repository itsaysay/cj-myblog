package com.cj.myblog.controller.admin;

import com.cj.myblog.controller.BaseController;
import com.cj.myblog.model.PartDo;
import com.cj.myblog.model.Result;
import com.cj.myblog.service.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static com.cj.myblog.constants.Constants.*;


/**
 * Created by jujunchen on 2017/8/29.
 */
@Controller
@RequestMapping("/admin/part/")
public class PartController extends BaseController{

    @Autowired
    private PartService partService;

    @RequestMapping(value = LIST,method = RequestMethod.GET)
    public void list(){
    }

    @ResponseBody
    @RequestMapping(value = PAGE,method = RequestMethod.POST)
    public Result<List<PartDo>> page(){
        Result<List<PartDo>> list = partService.findAll();
        return list;
    }

    @RequestMapping(value = ADD,method = RequestMethod.GET)
    public void add(){
    }

    @RequestMapping(value = EDIT,method = RequestMethod.GET)
    public void edit(ModelMap modelMap,Long id){
        PartDo partDo = partService.findOne(id);
        modelMap.put("bean",partDo);
    }

    @ResponseBody
    @RequestMapping(value = DELETE,method = RequestMethod.GET)
    public void delete(Long id){
        partService.delete(id);
    }

    @ResponseBody
    @RequestMapping(value = SAVE,method = RequestMethod.POST)
    public Result<String> save(PartDo partDo){
        Result<String> result = partService.save(partDo);
        return result;
    }


}
