package com.cj.myblog.controller.admin;

import com.cj.myblog.constants.Constants;
import com.cj.myblog.common.util.FileUtils;
import com.cj.myblog.common.util.UpYunUtil;
import com.cj.myblog.common.util.WebSetUtil;
import com.cj.myblog.controller.BaseController;
import com.cj.myblog.model.Result;
import com.cj.myblog.model.WebSiteDo;
import com.cj.myblog.plugin.PluginClassRegister;
import com.cj.myblog.service.SystemService;
import com.xiaoleilu.hutool.date.DateUtil;
import com.xiaoleilu.hutool.io.FileUtil;
import com.xiaoleilu.hutool.util.ClassUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import com.xiaoleilu.hutool.util.XmlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.*;

/**
 * Created by jujunchen on 2017/9/10.
 */
@Controller
@RequestMapping("/admin/system/")
public class SystemController extends BaseController{

    @Autowired
    private SystemService systemService;

    /**
     * 模板列表
     * @param modelMap
     */
    @GetMapping("tpl_list")
    public void tplList(ModelMap modelMap){
        List<Map<String,String>> tplList = new ArrayList<>();
        String root = ClassUtil.getClassPath() + "templates/front/theme/";
        File[] files = FileUtil.ls(root);
        for (File f:files) {
            if (f.isDirectory()){
                Map<String,String> map = new HashMap<>();
                String themexml = root + f.getName() + "/static/theme.xml";
                if (FileUtil.isFile(themexml)){
                    Document document = XmlUtil.readXML(themexml);
                    map.put("id",document.getElementsByTagName("id").item(0).getFirstChild().getNodeValue());
                    map.put("name",document.getElementsByTagName("name").item(0).getFirstChild().getNodeValue());
                    map.put("url",document.getElementsByTagName("url").item(0).getFirstChild().getNodeValue());
                    map.put("note",document.getElementsByTagName("note").item(0).getFirstChild().getNodeValue());
                    map.put("version",document.getElementsByTagName("version").item(0).getFirstChild().getNodeValue());
                    map.put("pubdate",document.getElementsByTagName("pubdate").item(0).getFirstChild().getNodeValue());
                    map.put("modified",document.getElementsByTagName("modified").item(0).getFirstChild().getNodeValue());
                    map.put("description",document.getElementsByTagName("description").item(0).getFirstChild().getNodeValue());
                    map.put("author", document.getElementsByTagName("author").item(0).getFirstChild().getNodeValue());
                }
                map.put("scree","/theme/" + f.getName() + "/static/screenshot.png");
                tplList.add(map);
            }
        }

        modelMap.put("list",tplList);
        modelMap.put("theme", WebSetUtil.getTheme());
    }

    /**
     * 模板保存
     * @param theme
     * @return
     */
    @ResponseBody
    @GetMapping("tpl_save")
    public Result<String> tplSave(String theme){
        Result<String> result = systemService.tplSave(theme);
        return result;
    }

    /**
     * 网站设置
     */
    @GetMapping("webset")
    public void webset(ModelMap modelMap){
        WebSiteDo webSiteDo = systemService.findOne(Constants.WEB_SET_ID);

        PluginClassRegister.register();

        modelMap.put("bean",webSiteDo);
    }

    /**
     * 网站设置保存
     */
    @ResponseBody
    @PostMapping("websetSave")
    public void websetSave(WebSiteDo webSiteDo){
        webSiteDo.setId(Constants.WEB_SET_ID);
        systemService.save(webSiteDo);
    }


    /**
     * 附件上传
     * @param request
     * @return
     */
    @RequestMapping(value = "upload")
    @ResponseBody
    public Map<String,Object> upload(HttpServletRequest request) {

        List<String> localPath= FileUtils.uploadFile(request,"file");

        List<String> uploadPath = new ArrayList<>();

        Map<String,Object> returnMap = new HashMap<>();

        for (String path:localPath) {
            String fileName = StrUtil.subSuf(path,path.lastIndexOf("/"));
            String antPath = "/upload" + "/file/"+ DateUtil.formatDate(new Date())+fileName;
            //上传到又拍云
            String remotePath = UpYunUtil.restAPI(antPath,path);
            if (StrUtil.isNotEmpty(remotePath)) {
                uploadPath.add(remotePath);
            }
        }

        if (uploadPath.size() == 0){

            returnMap.put("errno",9999);
        }else {
            returnMap.put("errno",0);
        }
        returnMap.put("data",uploadPath);

        return returnMap;
    }


}
