package com.cj.myblog.service.impl;

import com.cj.myblog.constants.ResultEnum;
import com.cj.myblog.dao.UserRepository;
import com.cj.myblog.model.Result;
import com.cj.myblog.model.UserDo;
import com.cj.myblog.service.UserService;
import com.xiaoleilu.hutool.crypto.SecureUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017-08-30.
 */
@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserRepository userRepository;

    @Override
    public Result<UserDo> findByUsernameAndPassword(UserDo userDo) {
        UserDo user = userRepository.findByUsernameAndPassword(userDo.getUsername(), SecureUtil.md5(userDo.getPassword()));
        if (user == null){
            return new Result<UserDo>(ResultEnum.USERNAME_PASSWORD_ERROR);
        }
        return new Result<>(user);
    }
}
