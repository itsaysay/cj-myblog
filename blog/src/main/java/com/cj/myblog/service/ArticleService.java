package com.cj.myblog.service;

import com.cj.myblog.model.ArticleDo;
import com.cj.myblog.model.PageBean;
import com.cj.myblog.model.Result;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface ArticleService {

    PageBean<ArticleDo> findAll(ArticleDo articleDo);

    void deleteByIdIn(List<Long> idList);

    ArticleDo findById(Long id);

    Result<String> save(ArticleDo articleDo);

    List<ArticleDo> frontFindAll(ArticleDo articleDo);

    ArticleDo frontFindById(Long id);
}
