package com.cj.myblog.service;

import com.cj.myblog.model.PageBean;
import com.cj.myblog.model.PluginDo;
import com.cj.myblog.model.Result;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface PluginService {

    PageBean<PluginDo> findAll(PluginDo pluginDo);

    PluginDo getUnInstallPlugin(String name);

    Result<String> installPlug(String skuNumber);

    List<PluginDo> findAllByStatus();

    void uninstall(Long id, String skuNumber);

    void save(PluginDo pluginDo);

    Result<String> export(Long id);
}
