package com.cj.myblog.service.impl;

import com.cj.myblog.dao.TagRepository;
import com.cj.myblog.model.Result;
import com.cj.myblog.model.TagDo;
import com.cj.myblog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public Result<List<TagDo>> findAll() {
        List<TagDo> list = tagRepository.findAllByTagNot("");

        return new Result<>(list);
    }

    @Override
    public Result<List<TagDo>> findByTagStartingWith(String tag) {
        List<TagDo> list = tagRepository.findByTagStartingWith(tag);
        return new Result<>(list);
    }


}


