package com.cj.myblog.service;

import com.cj.myblog.model.Result;
import com.cj.myblog.model.WebSiteDo;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface SystemService {

    Result<String> tplSave(String theme);

    void save(WebSiteDo webSiteDo);

    WebSiteDo findOne(Long webSetId);
}
