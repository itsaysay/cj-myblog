package com.cj.myblog.service;

import com.cj.myblog.model.Result;
import com.cj.myblog.model.TagDo;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface TagService {


    Result<List<TagDo>> findAll();

    Result<List<TagDo>> findByTagStartingWith(String tag);
}
