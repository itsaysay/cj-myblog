package com.cj.myblog.service;

import com.cj.myblog.model.PartDo;
import com.cj.myblog.model.Result;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface PartService {

    Result<List<PartDo>> findAll();

    Result<String> save(PartDo partDo);

    PartDo findOne(Long id);

    void delete(Long id);

    List<PartDo> findByStatus(int one);
}
