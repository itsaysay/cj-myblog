package com.cj.myblog.service;

import com.cj.myblog.model.CommentDo;
import com.cj.myblog.model.PageBean;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface CommentService {

    List<CommentDo> frontFindByArtId(Long id);

    void save(CommentDo commentDo);

    PageBean<CommentDo> findAll(CommentDo commentDo);

    void delete(Long id);

    CommentDo findOneById(Long id);
}
