package com.cj.myblog.service.impl;

import com.cj.myblog.constants.Constants;
import com.cj.myblog.common.util.CommUtil;
import com.cj.myblog.dao.WebSiteRepository;
import com.cj.myblog.model.Result;
import com.cj.myblog.model.WebSiteDo;
import com.cj.myblog.service.SystemService;
import com.xiaoleilu.hutool.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

/**
 * Created by Administrator on 2017-08-30.
 */
@Service
public class SystemServiceImpl implements SystemService {

    @Autowired
    private WebSiteRepository webSiteRepository;



    @PostConstruct
    public void init(){
        WebSiteDo webSiteDo = webSiteRepository.findOne(Constants.WEB_SET_ID);
        Constants.options = BeanUtil.beanToMap(webSiteDo);
    }


    @Override
    @Transactional
    public Result<String> tplSave(String theme) {
        Constants.options.put("theme",theme);
        WebSiteDo webSiteDo = webSiteRepository.findOne(Constants.WEB_SET_ID);
        webSiteDo.setTheme(theme);
        return new Result<>();
    }

    @Override
    @Transactional
    public void save(WebSiteDo webSiteDo) {
        WebSiteDo DO = webSiteRepository.findOne(webSiteDo.getId());
        BeanUtil.copyProperties(webSiteDo,DO, BeanUtil.CopyOptions.create().setIgnoreNullValue(true));
        //更新网站设置
        Constants.options = BeanUtil.beanToMap(DO);
    }


    @Override
    public WebSiteDo findOne(Long webSetId) {
        WebSiteDo webSiteDo = webSiteRepository.findOne(webSetId);
        webSiteDo.setMailTpl(CommUtil.htmlspecialchars(webSiteDo.getMailTpl() ==null?"":webSiteDo.getMailTpl()));
        return webSiteDo;
    }



}
