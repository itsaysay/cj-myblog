package com.cj.myblog.service;

import com.cj.myblog.model.Result;
import com.cj.myblog.model.UserDo;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface UserService {

    Result<UserDo> findByUsernameAndPassword(UserDo userDo);
}
