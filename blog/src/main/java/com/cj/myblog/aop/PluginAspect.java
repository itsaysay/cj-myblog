package com.cj.myblog.aop;

import com.cj.myblog.model.ArticleDo;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 *
 * @author: jujunchen
 * @description:  插件aop
 * @date: 2017-10-26 23:32:42
 *
 */
@Aspect
@Component
public class PluginAspect {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 切入文章管理保存方法
     */
    @Pointcut("execution(* com.cj.myblog.controller.admin.ArticleController.save(..))")
    public void articleAop(){}

    @Around("articleAop()")
    public Object beforeArticle(ProceedingJoinPoint point){

        Object[] args = point.getArgs();
        /*for (int i = 0; i < args.length; i++) {
            ArticleDo articleDo = (ArticleDo) args[i];
            articleDo.setTitle("测试aop修改参数");
        }*/

        try {
            Object object = point.proceed(args);
            return object;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }




}
