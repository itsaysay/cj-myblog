package com.cj.myblog.interceptor;

import com.cj.myblog.constants.Constants;
import com.cj.myblog.model.UserDo;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2017-08-30.
 */
public class SessionInterceptor implements HandlerInterceptor{


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        HttpSession session = request.getSession();
        if (session == null){
            response.sendRedirect("/admin/login");
            return false;
        }else {
            UserDo userDo = (UserDo) session.getAttribute(Constants.SESSION_KEY);
            if (userDo == null){
                response.sendRedirect("/admin/login");
                return false;
            }
            return true;
        }

    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
