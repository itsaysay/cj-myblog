package com.cj.myblog.interceptor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Administrator on 2017-08-30.
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Bean
    public WebMvcInterceptor webMvcInterceptor(){
        return new WebMvcInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //后台session过滤器
        registry.addInterceptor(new SessionInterceptor()).addPathPatterns("/admin/**").excludePathPatterns("/admin/login");
        //前台页面过滤器
        registry.addInterceptor(webMvcInterceptor()).addPathPatterns("/**");
        super.addInterceptors(registry);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/templates/plugins/**").addResourceLocations("classpath:/templates/plugins/");
        registry.addResourceHandler("/admin/static/**").addResourceLocations("classpath:/templates/admin/static/");
        registry.addResourceHandler("/theme/**").addResourceLocations("classpath:/templates/front/theme/");
        super.addResourceHandlers(registry);
    }

}
