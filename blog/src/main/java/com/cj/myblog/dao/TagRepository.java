package com.cj.myblog.dao;

import com.cj.myblog.model.TagDo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface TagRepository extends JpaRepository<TagDo,Long> {

    List<TagDo> findAllByTagNot(String s);

    TagDo findAllByTag(String tags);

    List<TagDo> findByTagStartingWith(String tag);

    List<TagDo> findAllByIdIn(List<Long> tags);
}
