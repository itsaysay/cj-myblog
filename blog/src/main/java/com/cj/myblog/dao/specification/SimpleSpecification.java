package com.cj.myblog.dao.specification;


import com.xiaoleilu.hutool.date.DateUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by konghao on 2016/12/15.
 */
public class SimpleSpecification<T> implements Specification<T> {

    /**
     * 查询的条件列表，是一组列表
     * */
    private List<SpecificationOperator> opers;

    public SimpleSpecification(List<SpecificationOperator> opers) {
        this.opers = opers;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        int index = 0;
        Predicate resultPre = null;
        for(SpecificationOperator op:opers) {
            Predicate pre = generatePredicate(root,criteriaBuilder,op);
            if(pre==null){
                continue;
            }else {
                if (index++ == 0){
                    resultPre = pre;
                    continue;
                }
                if("and".equalsIgnoreCase(op.getJoin())) {
                    resultPre = criteriaBuilder.and(resultPre,pre);
                } else if("or".equalsIgnoreCase(op.getJoin())) {
                    resultPre = criteriaBuilder.or(resultPre,pre);
                }
            }
        }

        return resultPre;
    }

    private Predicate generatePredicate(Root<T> root,CriteriaBuilder criteriaBuilder, SpecificationOperator op) {
        /*
        * 根据不同的操作符返回特定的查询*/
        if("=".equalsIgnoreCase(op.getOper()) && op.getValue() != null) {
            System.out.println(op.getKey()+","+op.getValue());
            return criteriaBuilder.equal(root.get(op.getKey()),op.getValue());
        } else if(">=".equalsIgnoreCase(op.getOper()) && op.getValue() != null) {
            return criteriaBuilder.ge(root.<Number>get(op.getKey()), (Number)op.getValue());
        } else if("<=".equalsIgnoreCase(op.getOper()) && op.getValue() != null) {
            return criteriaBuilder.le(root.<Number>get(op.getKey()),(Number)op.getValue());
        } else if(">".equalsIgnoreCase(op.getOper()) && op.getValue() != null) {
            return criteriaBuilder.gt(root.<Number>get(op.getKey()),(Number)op.getValue());
        } else if("<".equalsIgnoreCase(op.getOper()) && op.getValue() != null) {
            return criteriaBuilder.lt(root.<Number>get(op.getKey()),(Number)op.getValue());
        } else if(":".equalsIgnoreCase(op.getOper()) && op.getValue() != null) {
            return criteriaBuilder.like(root.<String>get(op.getKey()),"%"+op.getValue()+"%");
        } else if("l:".equalsIgnoreCase(op.getOper()) && op.getValue() != null) {
            return criteriaBuilder.like(root.<String>get(op.getKey()),op.getValue()+"%");
        } else if(":l".equalsIgnoreCase(op.getOper()) && op.getValue() != null) {
            return criteriaBuilder.like(root.<String>get(op.getKey()),"%"+op.getValue());
        } else if("null".equalsIgnoreCase(op.getOper())) {
            return criteriaBuilder.isNull(root.get(op.getKey()));
        } else if("!null".equalsIgnoreCase(op.getOper())) {
            return criteriaBuilder.isNotNull(root.get(op.getKey()));
        } else if("!=".equalsIgnoreCase(op.getOper())) {
            return criteriaBuilder.notEqual(root.get(op.getKey()),op.getValue());
        } else if ("between".equalsIgnoreCase(op.getOper()) && StrUtil.isNotEmpty((String)op.getValue()) && StrUtil.isNotEmpty((String)op.getValue())){
            return criteriaBuilder.between(root.<Date>get(op.getKey()), DateUtil.parseDate(op.getValue().toString()).toSqlDate(),DateUtil.parseDate(op.getValue2().toString()).toSqlDate());
        }
        return null;
    }

}
