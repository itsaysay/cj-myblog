package com.cj.myblog.dao.specification;

import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by konghao on 2016/12/15.
 */
public class SimpleSpecificationBuilder<T> {

    /**
     * 条件列表
     */
    private List<SpecificationOperator> opers;

    /**
     * 构造函数，初始化的条件是and
     */
    public SimpleSpecificationBuilder(String key,String oper,Object value,Object... value2) {
        SpecificationOperator so = new SpecificationOperator();
        so.setJoin("and");
        so.setKey(key);
        so.setOper(oper);
        so.setValue(value);
        so.setValue2(getVal(value2));
        opers = new ArrayList<SpecificationOperator>();
        opers.add(so);
    }

    public SimpleSpecificationBuilder() {
        opers = new ArrayList<SpecificationOperator>();
    }

    /**
     * 完成条件的添加
     * @return
     */
    private SimpleSpecificationBuilder add(String key,String oper,String join,Object value,Object... value2) {
        SpecificationOperator so = new SpecificationOperator();
        so.setKey(key);
        so.setValue(value);
        so.setValue2(getVal(value2));
        so.setOper(oper);
        so.setJoin(join);
        opers.add(so);
        return this;
    }

    /**
     * 添加or条件的重载
     * @return this，方便后续的链式调用
     */
    public SimpleSpecificationBuilder addOr(String key,String oper,Object value,Object... value2) {
        return this.add(key,oper,"or",value,getVal(value2));
    }

    /**
     * 添加and的条件
     * @return
     */
    public SimpleSpecificationBuilder add(String key,String oper,Object value,Object... value2) {
        return this.add(key,oper,"and",value,getVal(value2));
    }

    public Specification generateSpecification() {
        Specification<T> specification = new SimpleSpecification<T>(opers);
        return specification;
    }

    private Object getVal(Object... value){
        if (value != null && value.length >0){
            return value[0];
        }
        return null;
    }

}
