package com.cj.myblog.dao;

import com.cj.myblog.model.CommentDo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface CommentRepository extends JpaRepository<CommentDo,Long> {

    @Query("select cd from CommentDo cd where cd.artId = ?1 and cd.commentId is  null order by cd.createTime desc")
    List<CommentDo> findByArtId(Long id);

    @Query("select  count(1) from CommentDo cd where cd.artId = ?1 and cd.commentId is  null ")
    int getCountByArtId(Long id);

    @Query("select cd from CommentDo cd where cd.artId = ?1 and cd.commentId is not null order by cd.createTime asc ")
    List<CommentDo> findByArtIdHasCommentId(Long id);

    @Query(value = "select new CommentDo(c.id,c.comment,c.createTime,c.name,c.email,a.title) from CommentDo c,ArticleDo a where c.artId=a.id order by c.createTime desc")
    Page<CommentDo> findCommentDef(Pageable pageable);



}
