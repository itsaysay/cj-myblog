package com.cj.myblog.dao;

import com.cj.myblog.model.PluginDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface PluginRepository extends JpaRepository<PluginDo,Long> {

    @Query("select p from PluginDo p where p.status = 1 and p.isMain = 1")
    List<PluginDo> findAllByStatus();
}
