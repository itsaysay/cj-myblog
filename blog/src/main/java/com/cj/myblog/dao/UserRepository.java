package com.cj.myblog.dao;

import com.cj.myblog.model.UserDo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface UserRepository extends JpaRepository<UserDo,Long> {

    UserDo findByUsernameAndPassword(String username, String password);
}
