package com.cj.myblog.dao;

import com.cj.myblog.model.ArticleDef;
import com.cj.myblog.model.ArticleDo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface ArticleRepository extends JpaRepository<ArticleDo,Long>,JpaSpecificationExecutor<ArticleDo> {

    List<ArticleDo> findByCreateTimeBetween(Date beginTime, Date endTime);

    void deleteByIdIn(List<Long> idList);

    @Query("select count(1) from ArticleDo where partId = ?1")
    int findCountByPartId(Long id);

    @Query("select art from ArticleDo art where FIND_IN_SET(?1,art.tags)>0")
    Page<ArticleDo> findAllArticleByTag(String id, Pageable pageable);

    @Query("select new ArticleDef(art.partId,COUNT(art.id))  from ArticleDo art group by art.partId")
    List<ArticleDef>  findCountByPartIdGroup();

}
