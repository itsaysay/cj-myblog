package com.cj.myblog.dao;

import com.cj.myblog.model.ArticleContentDo;
import com.cj.myblog.model.ArticleDo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface ArticleContentRepository extends JpaRepository<ArticleContentDo,Long> {

    void deleteByArticleIdIn(List<Long> idList);
}
