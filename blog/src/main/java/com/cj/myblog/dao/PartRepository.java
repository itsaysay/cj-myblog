package com.cj.myblog.dao;

import com.cj.myblog.model.PartDo;
import com.cj.myblog.model.UserDo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface PartRepository extends JpaRepository<PartDo,Long> {

    List<PartDo> findByStatus(int one);
}
