package com.cj.myblog.dao;

import com.cj.myblog.model.WebSiteDo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 2017-08-30.
 */
public interface WebSiteRepository extends JpaRepository<WebSiteDo,Long> {

}
