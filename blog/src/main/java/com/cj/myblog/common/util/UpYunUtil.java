package com.cj.myblog.common.util;

import main.java.com.UpYun;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;

/**
 * Created by Administrator on 2017-09-11.
 */
public class UpYunUtil {

    public static Log logger = LogFactory.getLog(UpYunUtil.class);

    /**
     * 又拍云Rest API上传
     * @param antPath 上传路径
     * @param path  文件绝对路径
     * @return
     */
    public  static String restAPI(String antPath,String path){

        String upyunServiceName = "cj-blog";
        String upyunOper = "myblog";
        String upyunPass = "myblog123";
        String upyunPath = "http://cj-blog.b0.upaiyun.com";


        UpYun upYun = new UpYun(upyunServiceName,upyunOper,upyunPass);
        upYun.setDebug(false);
        upYun.setTimeout(30);
        upYun.setApiDomain(UpYun.ED_AUTO);

        boolean result = false;
        File file = new File(path);
        try {
            upYun.setContentMD5(UpYun.md5(file));
            result = upYun.writeFile(antPath,file,true);
        } catch (IOException e) {
           logger.error(e.getMessage());
        }

        if (result){
            return upyunPath + antPath;
        }else {
            return "";
        }
    }


}
