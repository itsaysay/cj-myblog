package com.cj.myblog.common.util;

import com.cj.myblog.constants.Constants;

/**
 * Created by Administrator on 2017-08-30.
 */
public class WebSetUtil {


    /**
     * 获取当前主题
     * @return
     */
    public static String getTheme() {
        String theme = Constants.options.get("theme").toString();
        return theme;
    }


    /**
     * 根据属性获得网站设置
     * @param key
     * @return
     */
    public static String getOption(String key){
        Object value = Constants.options.get(key);
        if ( value != null){
            return value.toString();
        }else {
            return "";
        }
    }


}
