package com.cj.myblog.common.util;

import com.xiaoleilu.hutool.io.file.FileWriter;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017-08-30.
 */
public class ConfUtil {

    private static String ymlPahth = "application.yml";

    /**
     * 获取yml配置
     * @param value
     * @return
     */
    public static String getYml(String value){
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(ymlPahth);
        Yaml yaml = new Yaml();
        Map<String,String> map = yaml.loadAs(inputStream,HashMap.class);
        return map.get(value);
    }


    public static void setYml(String key,String value) {
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(ymlPahth);
        Yaml yaml = new Yaml();
        Map<String,String> map = yaml.loadAs(inputStream,HashMap.class);
        map.put(key,value);
        String data = yaml.dump(map);
        FileWriter fileWriter = new FileWriter(ymlPahth);
        fileWriter.write(data);
    }


    public static void main(String[] args) {
           setYml("freemarker.front.tpl","tpl2");
    }



}
