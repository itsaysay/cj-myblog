package com.cj.myblog.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jujunchen on 2017/9/6.
 */
public class CommUtil {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * List<String>转List<Long>
     */
    public static List<Long> stringToLongList(List<String> strList){
        List<Long> longList = new ArrayList<>(strList.size());
        for (String str:strList){
            longList.add(Long.parseLong(str));
        }
        return longList;
    }


    /**
     * 转换html标签
     */

    public static String htmlspecialchars(String str){
        /*str = str.replaceAll("&", "&amp;");
        str = str.replaceAll("<", "&lt;");
        str = str.replaceAll(">", "&gt;");
        str = str.replaceAll("\"", "&quot;");*/
        str = str.replaceAll("\'", "\"");
        str  = str.replaceAll("\r","");
        str  = str.replaceAll("\n","<br>");
        return str;
    }



}
