#cj-myblog
基于spring-boot 、spring-data-jpa 的个人博客

用到的相关技术和资源：  

    1.IN+HTML后台管理模板  
    2.huTool工具集
    3.spring-boot
    4.spring-data-jpa
    5.bootstrap
    6.wangEditor  
    
`系统相关功能尚不稳定，目前正在开发插件相关功能`

![后台登录](https://gitee.com/uploads/images/2017/1027/001048_4a24eeaa_863058.png "QQ20171027-000935.png")  

![后台管理界面](https://gitee.com/uploads/images/2017/1027/001112_057e848d_863058.png "QQ20171027-001006.png")  

![默认前端模板](https://gitee.com/uploads/images/2017/1027/004113_0e0f4a8e_863058.png "QQ20171027-004137.png")  
