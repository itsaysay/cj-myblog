package com.cj.myblog.plugin;

import com.cj.myblog.constants.Constants;
import com.cj.myblog.model.PageBean;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: jujunchen
 * @description: 后台管理插件需要继承的接口
 * @date: 2017-10-26
 */
public interface PluginAdminController<T> {

    /**
     * 后台插件首页列表，插件第一个展示的页面
     */
    @GetMapping(Constants.INDEX)
    void index();


    /**
     * 后台插件列表分页方法
     * @param t
     * @return
     */
    @ResponseBody
    @PostMapping(Constants.PAGE)
    PageBean<T> page(T t);


    /**
     * 后台插件删除方法
     * @param id 要删除的id
     */
    @ResponseBody
    @GetMapping(Constants.DELETE)
    void delete(Long id);

    /**
     * 后台插件编辑方法
     * @param model
     * @param id 要编辑的id
     */
    @GetMapping(Constants.EDIT)
    void edit(ModelMap model, Long id);




}
