package com.cj.myblog.plugin;

/**
 * @author: jujunchen
 * @description:  插件开发接口
 * @date: 2017-10-26 23:32:42
 *
 */
public interface PluginAdminHandler {

    /**
     * 切入点方法之前执行
     * 如果不处理需要返回原参数
     * @param args 原方法入参
     * @return 返回给原方法参数
     */
    Object[] beforeHandler(Object[] args);


    /**
     * 切入点方法执行后执行
     * 如果不处理需要返回原参数
     * @param args 原方法执行后返回的结果，做为入参
     * @return 返回方法
     */
    Object[] afterHandler(Object[] args);


}
