package com.cj.myblog.constants;

import com.xiaoleilu.hutool.util.ClassUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Athor: jujun chen
 * @Description: 常量
 * @Date 2017-09-01
 */
public class Constants {

    /**
     * session key
     */
    public static final  String SESSION_KEY = "session_key";

    /**
     * controller index
     */
    public static final String INDEX = "index";

    /**
     * controller list
     */
    public static final String LIST = "list";

    /**
     * controller edit
     */
    public static final String EDIT = "edit";

    /**
     * controller add
     */
    public static final String ADD = "add";

    /**
     * controller view
     */
    public static final String VIEW = "view";

    /**
     * controller save
     */
    public static final String SAVE = "save";

    /**
     * controller delete
     */
    public static final String DELETE = "delete";

    /**
     * controller page
     */
    public static final String PAGE = "page";

    /**
     * 默认前台页面模板
     */
    public static final String THEME = "default";

    /**
     * 运行时插件目录
     */
    public static final String PLUG_ROOT = ClassUtil.getClassPath() + "plugins/";


    /**
     * 运行时插件页面目录
     */
    public static final String PLUG_TPL_ROOT = ClassUtil.getClassPath() + "plugins/";

    /**
     * 插件java源文件相对地址
     */
    public static final String PLUG_JAVA_SOURCE_ROOT = "/src/main/java/com/cj/myblog/plugins/";

    /**
     * 插件HTML源文件相对地址
     */
    public static final String PLUG_HTML_SOURCE_ROOT = "/src/main/resources/templates/plugins/";





    /**
     * 网站设置id
     */
    public static final Long WEB_SET_ID  = 1L;

    /**
     * one:1
     */
    public static final int ONE = 1;

    /**
     * tow:2
     */
    public static final int TWO = 2;

    /**
     * three:3
     */
    public static final int THREE = 3;

    /**
     * 网站系统配置
     */
    public static Map<String,Object> options = new HashMap<>();

    /**
     * 表情
     */
    public static List<Map> face = new ArrayList<>();


    /**
     * 后台插件模板
     */
    public static String PLUGIN_ADMIN_TPL = "package com.cj.myblog.plugins.{};\n" +
            "\n" +
            "import com.cj.myblog.controller.PluginAdminController;\n" +
            "import com.cj.myblog.model.PageBean;\n" +
            "import org.springframework.stereotype.Controller;\n" +
            "import org.springframework.ui.ModelMap;\n" +
            "import org.springframework.web.bind.annotation.RequestMapping;\n" +
            "\n" +
            "/**\n" +
            " * \n" +
            " * @author: {}\n" +
            " * @description: {}\n" +
            " * @date: {}\n" +
            " * \n" +
            " */\n" +
            "@Controller\n" +
            "@RequestMapping(\"/plugins/admin/{}/\")\n" +
            "public class PluginAdmin{} implements PluginAdminController {\n" +
            "\n" +
            "    @Override\n" +
            "    public void index() {}\n" +
            "\n" +
            "    @Override\n" +
            "    public PageBean page(Object o) {\n" +
            "        return null;\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public void delete(Long id) {}\n" +
            "\n" +
            "    @Override\n" +
            "    public void edit(ModelMap model, Long id) {}\n" +
            "}\n";


    /**
     * 前端index.ftl模板文件
     */
    public static final String PLUGIN_INDEX_TPL = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>Title</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "test\n" +
            "</body>\n" +
            "</html>";


}
