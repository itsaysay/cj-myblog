package com.cj.myblog.constants;

/**
 * Created by Administrator on 2017-08-31.
 */
public enum ResultEnum{

    SUCCESS(0,""),
    OTHER_ERROR(9999,"其他错误"),
    SAVE_ERROR(9998, "保存失败"),
    USERNAME_PASSWORD_ERROR(1000,"用户名或密码不正确"),
    VERIFYCODE_ERROR(1001,"验证码不正确"),
    PLUG_INSTALL_ERROR(1002,"插件安装失败");



    private int code;
    private String msg;

    ResultEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }


    public String getMsg() {
        return msg;
    }


    //根据index获取value
    public static String getValue(int code){
        for (ResultEnum c : ResultEnum.values()) {
            if (c.code == code) {
                return c.msg;
            }
        }
        return "";
    }



}
