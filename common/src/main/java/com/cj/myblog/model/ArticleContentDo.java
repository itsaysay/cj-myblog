package com.cj.myblog.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2017-05-10.
 */
@Entity
@Table(name = "sys_article_content")
public class ArticleContentDo implements Serializable{


    @Column(name = "article_id")
    @Id
    private Long articleId;

    @Column(name = "content")
    private String content;

    @Column(name = "directory")
    private String directory;


    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
