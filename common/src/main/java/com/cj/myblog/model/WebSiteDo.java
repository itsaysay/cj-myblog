package com.cj.myblog.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Administrator on 2017-05-10.
 */
@Entity
@Table(name = "sys_website")
public class WebSiteDo  implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "theme")
    private String theme;

    @Column(name = "title")
    private String title;

    @Column(name = "key_words")
    private String keyWords;

    @Column(name = "description")
    private String description;

    @Column(name = "footer")
    private String footer;

    @Column(name = "blog_name")
    private String blogName;

    @Column(name = "blog_subname")
    private String blogSubname;

    @Column(name = "url")
    private String url;

    @Column(name = "mail_server")
    private String mailServer;

    @Column(name = "mail_user")
    private String mailUser;

    @Column(name = "mail_password")
    private String mailPassword;

    @Column(name = "mail_tpl")
    private String mailTpl;

    @Column(name = "dev_mode")
    private int devMode = 0;

    @Column(name = "dev_path")
    private String devPath;


    @Column(name = "footer_other")
    private String footerOther;



    public String getFooterOther() {
        return footerOther;
    }

    public void setFooterOther(String footerOther) {
        this.footerOther = footerOther;
    }

    public int getDevMode() {
        return devMode;
    }

    public void setDevMode(int devMode) {
        this.devMode = devMode;
    }

    public String getDevPath() {
        return devPath;
    }

    public void setDevPath(String devPath) {
        this.devPath = devPath;
    }

    public String getMailTpl() {
        return mailTpl;
    }

    public void setMailTpl(String mailTpl) {
        this.mailTpl = mailTpl;
    }

    public String getMailServer() {
        return mailServer;
    }

    public void setMailServer(String mailServer) {
        this.mailServer = mailServer;
    }

    public String getMailUser() {
        return mailUser;
    }

    public void setMailUser(String mailUser) {
        this.mailUser = mailUser;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    public void setMailPassword(String mailPassword) {
        this.mailPassword = mailPassword;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public String getBlogSubname() {
        return blogSubname;
    }

    public void setBlogSubname(String blogSubname) {
        this.blogSubname = blogSubname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }


}
