package com.cj.myblog.model;

/**
 * Created by Administrator on 2017-08-30.
 */

import com.cj.myblog.constants.ResultEnum;

import java.io.Serializable;

/**
 * 结果返回bean
 */
public class Result<T> implements Serializable {

    //错误吗
    private int code = ResultEnum.SUCCESS.getCode();

    //错误信息
    private String msg = ResultEnum.SUCCESS.getMsg();

    //返回实体
    private T data;

    public Result() {
    }

    public Result(ResultEnum resultEnum, T data) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
        this.data = data;
    }

    public Result(ResultEnum resultEnum) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
    }

    public Result(T data) {
        this.data = data;
    }



    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public  boolean isSuccess(){
        if (this.code == 0) {
            return true;
        }
        return false;
    }

}
