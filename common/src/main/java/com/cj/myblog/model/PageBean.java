package com.cj.myblog.model;

import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

/**
 * 整合页面分页插件
 */
public class PageBean<T> implements Serializable {

	private static final long serialVersionUID = 6975089268517363694L;

	// 当前记录
	private int iDisplayStart;

	// 每页记录数
	private int iDisplayLength;

	// 总记录数
	private int iTotalRecords;

	// 过滤后记录
	private int iTotalDisplayRecords;

	// 记录
	private List<T> data;

	public PageBean() {
	}
	
	public PageBean(Page<T> page) {
		this.iDisplayStart = page.getNumber();
		this.iDisplayLength = page.getSize();
		this.iTotalRecords = (int) page.getTotalElements();
		this.iTotalDisplayRecords = (int) page.getTotalElements();
		this.data = page.getContent();
	}

	public PageBean(int iDisplayStart, int iDisplayLength, int iTotalRecords, List<T> data) {
		this.iDisplayStart = iDisplayStart;
		this.iDisplayLength = iDisplayLength;
		this.iTotalRecords = iTotalRecords;
		this.iTotalDisplayRecords = iTotalRecords;
		this.data = data;
	}

	public int getiDisplayStart() {
		return iDisplayStart;
	}

	public void setiDisplayStart(int iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}

	public int getiDisplayLength() {
		return iDisplayLength;
	}

	public void setiDisplayLength(int iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

}
