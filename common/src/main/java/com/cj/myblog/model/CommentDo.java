package com.cj.myblog.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017-05-10.
 */
@Entity
@Table(name = "sys_comment")
public class CommentDo implements Serializable{

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "art_id")
    private Long artId;

    @Column(name = "comment_id")
    private Long commentId;

    @Column(name = "comment")
    private String comment;

    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone="GMT+8")
    private Date createTime;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Transient
    private String avatar;

    @Transient
    private String verifyCode;

    @Transient
    private  int start = 0;

    @Transient
    private int length = 10;

    @Transient
    private String artName;

    @Transient
    private List<CommentDo> commentDos;

    @Transient
    private int sendMail;

    @Transient
    private String toUser;

    @Transient
    private String toName;


    public CommentDo() {
    }

    public CommentDo(Long id,String comment, Date createTime, String name, String email, String artName) {
        this.id = id;
        this.comment = comment;
        this.createTime = createTime;
        this.name = name;
        this.email = email;
        this.artName = artName;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public int getSendMail() {
        return sendMail;
    }

    public void setSendMail(int sendMail) {
        this.sendMail = sendMail;
    }

    public List<CommentDo> getCommentDos() {
        return commentDos;
    }

    public void setCommentDos(List<CommentDo> commentDos) {
        this.commentDos = commentDos;
    }

    public String getArtName() {
        return artName;
    }

    public void setArtName(String artName) {
        this.artName = artName;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getArtId() {
        return artId;
    }

    public void setArtId(Long artId) {
        this.artId = artId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }



}
