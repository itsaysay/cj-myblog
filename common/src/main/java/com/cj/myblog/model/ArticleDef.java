package com.cj.myblog.model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Administrator on 2017-09-19.
 */
@Entity
public class ArticleDef {

    @Id
    private Long partId;

    private Long count;


    public ArticleDef(Long partId, Long count) {
        this.partId = partId;
        this.count = count;
    }

    public Long getPartId() {
        return partId;
    }

    public void setPartId(Long partId) {
        this.partId = partId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }


}
