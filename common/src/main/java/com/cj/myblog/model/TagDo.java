package com.cj.myblog.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Administrator on 2017-09-07.
 */
@Entity
@Table(name = "sys_tags")
public class TagDo implements Serializable{

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "tag")
    private String tag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
