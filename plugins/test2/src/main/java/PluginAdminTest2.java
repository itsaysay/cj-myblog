import com.cj.myblog.model.PageBean;
import com.cj.myblog.plugin.PluginAdminController;
import org.apache.commons.codec.digest.Md5Crypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: jujun chen
 * @Description:
 * @Date: 2017/10/29
 */
@Controller
@RequestMapping("/plugins/admin/test2/")
public class PluginAdminTest2 implements PluginAdminController {

    @Override
    public void index() {
        System.out.println(Md5Crypt.apr1Crypt("admin"));

    }


    @Override
    public PageBean page(Object o) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void edit(ModelMap model, Long id) {

    }


}
